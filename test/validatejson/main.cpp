/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <stdexcept>
#include <iostream>
#include <fstream>

#include <SCTBModbusDevice.h>
#include <json/json.h>

using namespace SCTBModbusDevice;

int main(int argc, char* argv[])
{
    for(int i = 1; i < argc; ++i)
    {
        String filename(argv[i]);
        std::cout << "Reading file: " << filename;
        std::filebuf fb;
        if (!fb.open(filename, std::ios::in))
        {
            std::cout << "... Can't open the file!" << std::endl;
            return -1;
        }
        std::istream is(&fb);

        Json::Value root; // will contains the root value after parsing.
        Json::Reader reader;
        bool parsingSuccessful = reader.parse( is, root );
        if ( !parsingSuccessful )
        {
            // report to the user the failure and their locations in the document.
            std::cout << "... Validation failed! "
                      << reader.getFormattedErrorMessages() << std::endl;
            fb.close();
            return -1;
        }
        else
        {
            std::cout << "... Validation successful!" << std::endl;
            fb.close();
        }
    }
    return 0;
}
