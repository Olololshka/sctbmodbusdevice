<h2>Библиотека доступа к устройствам СКТБ "ЭЛПА" по протоколу modbus</h2>

<b>Требуется:</b>
    * >=cmake-3.1
    * >=doxygen-1.8 (Документация)
    * >=git-1.7 (Получение исходных кодов)

<b>В проекте использованы следующие сторонние библиотеки:</b>
    * libmodbus модифицировани
        https://github.com/ololoshka2871/libmodbus.git
    * qserialdevice использован модуль qserialdeviceenumerator
        https://gitorious.org/qserialdevice
    * pthreads-win32 мьютексы для win32
        https://www.sourceware.org/pthreads-win32
    * jsoncpp карты памяти устройств
        https://github.com/open-source-parsers/jsoncpp
	* dirent.h for windows
		http://www.softagalleria.net/dirent.php

<b>Сборка:</b>
    * Скачать исхохный код при помощи git
    $ git clone https://Olololshka@bitbucket.org/Olololshka/sctbmodbusdevice.git

    * Создаем каталог для сборки
    $ mkdir sctbmodbusdevice/build && cd sctbmodbusdevice/build

    * запуск cmake
    ** для windows:
        ** С использованием MinGW
            $ cmake .. -G"MSYS Makefiles"
        ** C использованием MSVC
            > cmake ..
    ** для Linux:
            $ cmake ..
      В процессе могут возниукнуть ошибки, по причине нехватки некоторых обязательных компанентов.
      Воспользуйтесь документацией чтобы устранить эти ошибки.

    * Сборка
    cmake --build .

    * Установка
    cmake --build . --target install
    Для выполнения могут потребоваться повышенные привилегии.

    * Сборка документации (требуется наличие doxygen)
    cmake --build . --target doc

<b>Примеры: Смотри каталог demo</b>
	
<b>Связвание с клиентским приложеинем:</b>
	Библиотека поддерживает автоматическую настройку через или cmake.
		В случае использования сmake в windows укажите путь поиска при конфигурации проекта:
		-DCMAKE_PREFIX_PATH=<путь установки библиотеки>
		По-умолчанию C:\Program Files (x86)\SCTBModbusDevice\lib\cmake
		
