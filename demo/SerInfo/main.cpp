/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

/**
 * Данный пример показывает работу с классом
 * SCTBModbusDevice::SerialDeviceEnumerator для обнаружения и получения
 * информации об последовательных устройствах в системе
 */

#include <stdexcept>
#include <iostream>

#include <SerialDeviceEnumerator.h>

#define _P(Text, val)   std::cout << Text << val << std::endl

void showDeviceInfo(SCTBModbusDevice::StringList List)
{
    SCTBModbusDevice::SerialDeviceEnumerator *enumerator =
                SCTBModbusDevice::SerialDeviceEnumerator::instance();
    SCTBModbusDevice::StringList::const_iterator it = List.begin();
    while (it != List.end())
    {
        /* Установка имяни устройства для получения данных */
        enumerator->setDeviceName(*it++);
        _P(">>> info about: ", enumerator->name());
        _P("-> description  : ", enumerator->description());
        _P("-> driver       : ", enumerator->driver());
        _P("-> friendlyName : ", enumerator->friendlyName());
        _P("-> hardwareID : ", enumerator->hardwareID().join(" "));
        _P("-> locationInfo : ", enumerator->locationInfo());
        _P("-> manufacturer : ", enumerator->manufacturer());
        _P("-> service : ", enumerator->service());
        _P("-> shortName : ", enumerator->shortName());
        _P("-> subSystem : ", enumerator->subSystem());
        _P("-> systemPath : ", enumerator->systemPath());

        _P("-> vendorID : 0x", std::hex << enumerator->vendorID());
        _P("-> productID : 0x", std::hex << enumerator->productID());

        _P("-> revision : ", enumerator->revision());
        _P("-> bus : ", enumerator->bus());

        _P("-> is exists : ", enumerator->isExists());
        _P("-> is busy  : ", enumerator->isBusy());
    }
}

int main(int argc, char* argv[])
{
    // Создаем перечислитель
    SCTBModbusDevice::SerialDeviceEnumerator *enumerator =
            SCTBModbusDevice::SerialDeviceEnumerator::instance();
    // Получаем список последовательных устройств в системе
    SCTBModbusDevice::StringList avalable = enumerator->devicesAvailable();
    // выводим информацию об найденных устройствах в консоль
    showDeviceInfo(avalable);
    // унечтожаем перечислитель
    SCTBModbusDevice::SerialDeviceEnumerator::release();

    return 0;
}
