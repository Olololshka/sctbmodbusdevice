/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

/**
 * Данный пример показывает работу с классом SCTBModbusDevice::CellQuery
 * для выборки списка ячеек по параметрам
 */

#include <stdexcept>
#include <iostream>
#include <sstream>

#include <DeviceFactory.h>
#include <cell.h>
#include <CellQuery.h>

using namespace SCTBModbusDevice;

/* Печать списка ячеек в консоль */
void print_cells(const List<AbstractCell*>& list)
{
    for (List<AbstractCell *>::const_iterator it = list.begin();
         it != list.end(); ++it)
        std::cout << (*it)->toString() << std::endl;
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: device <devid>" << std::endl;
        return -1;
    }

    /* Получаем ID устройства с ячейками которого будем работать */
    uint16_t id;
    std::stringstream ss;
    ss << std::hex << argv[1];
    ss >> id;

    // Создаем устройство с указанным ID
    Device *dev = DeviceFactory::create(id);

    // печать сводки по устройству
    std::cout << "Devce info:" << std::endl;
    std::cout << "\t-> DevID: " << std::hex << dev->id() << std::dec << std::endl
              << "\t-> Class: " << dev->Class() << std::endl
              << "\t-> Description: " << dev->Description() << std::endl << std::endl;

    // создаем экземпляр класса-запроса
    CellQuery query;

    // Выберем из устройства все ячейки класса InputRegister
    std::cout << "Quering input cells:" << std::endl;
    query.Type = AbstractCell::InputRegister;
    print_cells(query(*dev));

    // Выберем из устройства все ячейки из категории "Флаги ощибок"
    std::cout << std::endl << "Quering error flags:" << std::endl;
    query.Type = AbstractCell::INVALID;
    query.Category = "Флаги ощибок";
    print_cells(query(*dev));

    // Выберем из устройства только те ячейки к которым запрещен групповой
    // доступ
    std::cout << std::endl << "Quering unserialisable cells:" << std::endl;
    query.Serialisable = 0;
    query.Category.clear();
    print_cells(query(*dev));

    // Выберем из устройства ячейки, содержащие яначения с плавающей точкой
    std::cout << std::endl << "Quering cells float type:" << std::endl;
    query.Serialisable = -1;
    query.valueType = typeid(float);
    print_cells(query(*dev));

    // Выберем из устройства ячейки, которые имеют тип bool (флаг)
    std::cout << std::endl << "Quering hiden boolean cells:" << std::endl;
    query.Category = "Hiden";
    query.valueType = typeid(bool);
    print_cells(query(*dev));

    // Выберем из устройства ячейки, которые содержат в имяни часть 'Част'
    std::cout << std::endl << "Quering cell by name fragment \"Част\":" << std::endl;
    query = CellQuery();
    query.Name = "Част";
    query.NameFragment = true;
    print_cells(query(*dev));

    delete dev;

    return 0;
}
