/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

/**
 * Данный пример показывает создание устройства по карте памяти с известным ID
 * и печатает в консоль данные по всем его ячейкам
 */

#include <stdexcept>
#include <iostream>
#include <sstream>

#include <DeviceFactory.h>
#include <cell.h>

using namespace SCTBModbusDevice;

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: device <devid>" << std::endl;
        return -1;
    }

    /* Получаем ID устройства с ячейками которого будем работать */
    uint16_t id;
    std::stringstream ss;
    ss << std::hex << argv[1];
    ss >> id;

    // Создаем устройство с указанным ID
    Device *dev = DeviceFactory::create(id);

    // печать сводки по устройству
    std::cout << "Devce info:" << std::endl;
    std::cout << "\t-> DevID: " << std::hex << dev->id() << std::dec << std::endl
              << "\t-> Class: " << dev->Class() << std::endl
              << "\t-> Description: " << dev->Description() << std::endl << std::endl;


    // Обходим список ячеек устройства и печатаем информацию по каждой ячейке
    for (List<AbstractCell *>::const_iterator it = dev->Cells().begin();
         it != dev->Cells().end(); ++it)
        std::cout << (*it)->toString() << std::endl;

    delete dev;

    return 0;
}
