#--------------------------------------------------
# cmake old/new compatibility
#--------------------------------------------------
cmake_minimum_required(VERSION 3.1)
	
# указать путь до модулей
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
	"${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules/")

project(SCTBModbusDevice)

Set(MEMMAP_ID_CELL_NAME "Идентификатор"
    CACHE STRING "Название ячейки с идентификатором" FORCE)
Set(MEMMAP_ADDRESS_CELL_NAME "Адрес"
    CACHE STRING "Название ячейки с адресом modbus" FORCE)
Set(MEMMAP_WRITE_SETTINGS_CELL_NAME "Запись настроек"
    CACHE STRING "Название катушки, управляющей записью настроек" FORCE)
Set(MEMMAP_SET_SETTINGS_CELL_NAME "Изменение настроек"
    CACHE STRING "Название катушки, управляющей применением настроек" FORCE)
Set(MEMMAP_HIDEN_CELL_CATEGORY_NAME "Hiden"
    CACHE STRING "Название категории скрытых настроек" FORCE)
Set(MEMMAP_PASSWD_CELL_NAME "Пароль доступа"
    CACHE STRING "Название ячейки с паролем доступа" FORCE)
Set(ROOT_PASSWD 0xAB01
    CACHE STRING "Пароль доступа" FORCE)

set(DESCRIPTION
    "SCTB \"ELPA\" Devices access library via modbus protocol")
file(STRINGS ${PROJECT_SOURCE_DIR}/VERSION
    VERSION LIMIT_COUNT 1)
#set(VERSION
#    "0.0.0-git")
set(PRJ_URL
    "https://bitbucket.org/Olololshka/sctbmodbusdevice")

OPTION(SCTBMbD_WITH_PKGCONFIG_SUPPORT
        "Generate and install .pc files" 		OFF)
OPTION(SCTBMbD_WITH_CMAKE_PACKAGE
	"Generate and install cmake package files" 	ON)

set(LIBMODBUS_GIT_REPO  https://github.com/ololoshka2871/libmodbus.git)

set(LIBMODBUS_GIT_BRANCH master)

set(3RD_PARTY_LIBS)
set(3RD_PARTY_INCLUDES)

include(ExternalProject)
if(WIN32)
    find_package(git REQUIRED)

    find_package(Threads)
    if(Threads_FOUND AND CMAKE_USE_PTHREADS_INIT)
        list(APPEND 3RD_PARTY_LIBS ${CMAKE_THREAD_LIBS_INIT})
        list(APPEND 3RD_PARTY_INCLUDES ${THREADS_INCLUDE_DIR})
        message(STATUS "Found win32 pthreads implementation")
    else(Threads_FOUND AND CMAKE_USE_PTHREADS_INIT)
        include(CheckStructHasMember)
        CHECK_STRUCT_HAS_MEMBER(
                    "struct timespec"
                    tv_sec
                    "time.h"
                    _HAVE_STRUCT_TIMESPEC
                    )
        if(_HAVE_STRUCT_TIMESPEC)
            set(_Pthreads_patch "0001-removing-library-name-suffixies-added-macroses.patch")
        else(_HAVE_STRUCT_TIMESPEC)
            set(_Pthreads_patch "0001-removing-library-name-suffixies.patch")
        endif(_HAVE_STRUCT_TIMESPEC)
        ExternalProject_add(pthreads-win32_build
	    DOWNLOAD_DIR    ${CMAKE_BINARY_DIR}
	    GIT_REPOSITORY  https://github.com/martell/pthreads-win32.cmake.git
	    GIT_TAG	    master
	    CMAKE_ARGS	    -DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_BINARY_DIR}
			    -DCMAKE_VERBOSE_MAKEFILE=${CMAKE_VERBOSE_MAKEFILE}
			    -DPTHREADS_BUILD_STATIC=1
			    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
            # Приходится использовать такие костыли, ибо студия говняется
            UPDATE_COMMAND   ${GIT_EXECUTABLE} reset --hard HEAD && ${GIT_EXECUTABLE} apply --ignore-whitespace "${CMAKE_CURRENT_SOURCE_DIR}/patches/${_Pthreads_patch}"
            )

        add_library(pthreads-win32 STATIC IMPORTED)
	
        set_property(TARGET pthreads-win32 PROPERTY IMPORTED_LOCATION
            ${CMAKE_BINARY_DIR}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}pthreads${CMAKE_STATIC_LIBRARY_SUFFIX})

        add_dependencies(pthreads-win32 pthreads-win32_build)

        list(APPEND 3RD_PARTY_INCLUDES ${CMAKE_BINARY_DIR}/include)
        list(APPEND 3RD_PARTY_LIBS pthreads-win32)
        add_definitions(-DPTW32_BUILD)
        if(_HAVE_STRUCT_TIMESPEC)
            add_definitions(-DHAVE_STRUCT_TIMESPEC -D_TIMESPEC_DEFINED)
        endif(_HAVE_STRUCT_TIMESPEC)
    endif(Threads_FOUND AND CMAKE_USE_PTHREADS_INIT)

    ExternalProject_add(libmodbus_build
	    DOWNLOAD_DIR    ${CMAKE_BINARY_DIR}
	    GIT_REPOSITORY  ${LIBMODBUS_GIT_REPO}
	    GIT_TAG	    ${LIBMODBUS_GIT_BRANCH}
	    UPDATE_COMMAND  ""
	    CMAKE_ARGS	    -DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_BINARY_DIR}
			    -DCMAKE_VERBOSE_MAKEFILE=${CMAKE_VERBOSE_MAKEFILE}
			    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
            )
    add_library(libmodbus STATIC IMPORTED)
	
    set_property(TARGET libmodbus PROPERTY IMPORTED_LOCATION
        ${CMAKE_BINARY_DIR}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}modbus${CMAKE_STATIC_LIBRARY_SUFFIX})

    add_definitions(
        -D_CRT_SECURE_NO_WARNINGS # скрыть варнинги студии по поводу стандартной либы
        -DDLLBUILD # решает проблему с __imp__{$funname}
        )
    set(ADD_LIBS
        wsock32
        ws2_32
        setupapi
        )
    set(SER_DEV_ENUMERATOR_SRC serialdeviceenumerator_p_win.cpp)
    set(VARIANT_PRV_H Variant.h)
    set(VARIANT_PRV_SRC Variant.cpp)
    list(APPEND 3RD_PARTY_LIBS libmodbus
        )
    if(MSVC)
        # костыли для студии
        list(APPEND 3RD_PARTY_INCLUDES msvc)
    endif(MSVC)
else(WIN32)
    find_package(Threads REQUIRED)

    set(3RD_PARTY_LIBS ${CMAKE_THREAD_LIBS_INIT})
    set(3RD_PARTY_INCLUDES ${THREADS_INCLUDE_DIR})

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        set(debug_flag -g)
    endif()

    set(LIBMODBUS_CONFIGURE_CMD
	cd <SOURCE_DIR> && ./autogen.sh && cd <BINARY_DIR> &&
	<SOURCE_DIR>/configure -disable-shared --prefix=${CMAKE_BINARY_DIR}
	CFLAGS=-fPIC\ ${CMAKE_C_FLAGS}\ ${debug_flag}
	CC=${CMAKE_C_COMPILER}
    )

    ExternalProject_add(libmodbus_build
            DOWNLOAD_DIR            ${CMAKE_BINARY_DIR}
            GIT_REPOSITORY          ${LIBMODBUS_GIT_REPO}
            GIT_TAG                 ${LIBMODBUS_GIT_BRANCH}
	    UPDATE_COMMAND          ""
            CONFIGURE_COMMAND       ${LIBMODBUS_CONFIGURE_CMD}
            BUILD_COMMAND           ${MAKE}
            )
    add_library(libmodbus STATIC IMPORTED)
    set_property(TARGET libmodbus PROPERTY IMPORTED_LOCATION
		${CMAKE_BINARY_DIR}/lib/libmodbus${CMAKE_STATIC_LIBRARY_SUFFIX})

    find_package( UDEV REQUIRED)
    set(ADD_LIBS ${UDEV_LIBRARIES})
    list(APPEND 3RD_PARTY_LIBS libmodbus)

    SET(SER_DEV_ENUMERATOR_SRC serialdeviceenumerator_p_unix.cpp)
endif(WIN32)
add_dependencies(libmodbus libmodbus_build)

add_subdirectory(memmaps)

set(JSON_CPP_INCLUDES)

find_package(JsonCpp)
if(JSONCPP_FOUND)
    set(JSON_CPP_INCLUDES ${JSONCPP_INCLUDE_DIRS})
    message(STATUS "Using installed libjsoncpp: ${JSONCPP_LIBRARIES}")
else(JSONCPP_FOUND)
    message(STATUS "Building own libjsoncpp")
    ExternalProject_add(libjsoncpp_build
	    DOWNLOAD_DIR    ${CMAKE_BINARY_DIR}
	    GIT_REPOSITORY  https://github.com/open-source-parsers/jsoncpp.git
            GIT_TAG	    master
	    UPDATE_COMMAND  ""
	    CMAKE_ARGS	    
			-DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_BINARY_DIR}
			-DCMAKE_VERBOSE_MAKEFILE=${CMAKE_VERBOSE_MAKEFILE}
			-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
            #		-DJSONCPP_LIB_BUILD_STATIC=ON
            #           -DJSONCPP_LIB_BUILD_SHARED=ON
                        -DBUILD_STATIC_LIBS=ON
                        -DBUILD_SHARED_LIBS=ON
			-DJSONCPP_WITH_CMAKE_PACKAGE=ON
			-DJSONCPP_WITH_TESTS=OFF
			-DJSONCPP_WITH_POST_BUILD_UNITTEST=OFF
			-DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
			-DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
	    #UPDATE_COMMAND  ${GIT_EXECUTABLE} reset --hard HEAD &&
	    #                ${GIT_EXECUTABLE} apply --ignore-whitespace
	    #                ${CMAKE_CURRENT_SOURCE_DIR}/patches/0001-pathfix.patch
            )
    #STATIC-либа для STATIC таргета, SHARED - для SHARED
    add_library(libjsoncpp_static STATIC IMPORTED)
    add_library(jsoncpp SHARED IMPORTED)
    link_directories("${CMAKE_BINARY_DIR}/bin")

    set_property(TARGET libjsoncpp_static PROPERTY IMPORTED_LOCATION
            "${CMAKE_BINARY_DIR}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}jsoncpp${CMAKE_STATIC_LIBRARY_SUFFIX}")

    if(WIN32)
        set(_l "${CMAKE_BINARY_DIR}/bin")
    else(WIN32)
        set(_l "${CMAKE_BINARY_DIR}/lib")
    endif(WIN32)

    set_property(TARGET jsoncpp PROPERTY
            IMPORTED_LOCATION   "${_l}/${CMAKE_SHARED_LIBRARY_PREFIX}jsoncpp${CMAKE_SHARED_LIBRARY_SUFFIX}")
    if(WIN32)
    set_property(TARGET jsoncpp PROPERTY
            IMPORTED_IMPLIB     "${CMAKE_BINARY_DIR}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}jsoncpp${CMAKE_STATIC_LIBRARY_SUFFIX}"
            )
    endif(WIN32)
			
    add_dependencies(libjsoncpp_static libjsoncpp_build)
    add_dependencies(jsoncpp libjsoncpp_build)
    set(JSON_CPP_INCLUDES ${CMAKE_BINARY_DIR}/include)
endif(JSONCPP_FOUND)

list(APPEND 3RD_PARTY_INCLUDES ${JSON_CPP_INCLUDES})

set(MEMMAP_PATH_STANDART
    ${CMAKE_INSTALL_PREFIX}/share/${PROJECT_NAME}/memmaps)
set(MEMMAP_PATH_USER ~/.${PROJECT_NAME}/memmaps)
configure_file(config.h.in ${CMAKE_CURRENT_BINARY_DIR}/config.h)

#инклюды для сборки (-I<путь>)
include_directories (
    .
    ${CMAKE_BINARY_DIR}/include/modbus
    ${3RD_PARTY_INCLUDES}
    ${CMAKE_CURRENT_BINARY_DIR}
    )

set(REQUEST_HADERS
    requests/read_holding_registers.h
    requests/read_input_registers.h
    requests/read_coils.h
    requests/read_discrete_inputs.h
    requests/write_holding_registers.h
    requests/write_coils.h
    requests/custom_request.h
    requests/write_single_coil.h
    )

#список хедеров проекта (не Q_OBJECT'ов!)
set(${PROJECT_NAME}_HDRS
    SCTBModbusDevice.h
    Connection.h
    request.h
    Prober.h
    Device.h
    DeviceFactory.h
    CellQuery.h

    abstractcell.h
    cell.h

    SerialDeviceEnumerator.h
    )

set(${PROJECT_NAME}_HDRS_PRV
    ${CMAKE_CURRENT_BINARY_DIR}/config.h

    CellFactory.h

    common.h
    serialdeviceenumerator_p.h

    ${REQUEST_HADERS}
    ${CELLS_HADERS}
    ${VARIANT_PRV_H}
    )

#список сырцов
set(${PROJECT_NAME}_SRCS
    Connection.cpp
    request.cpp
    Prober.cpp
    DeviceFactory.cpp
    CellFactory.cpp

    requests/read_holding_registers.cpp
    requests/read_input_registers.cpp
    requests/read_coils.cpp
    requests/read_discrete_inputs.cpp
    requests/write_holding_registers.cpp
    requests/write_coils.cpp
    requests/custom_request.cpp
    requests/write_single_coil.cpp
    )
#сырцы, в которых не используются инклюды от external project-ов
set(clear_SRC
    common.cpp
    SCTBModbusDevice.cpp

    abstractcell.cpp
    Device.cpp
    CellQuery.cpp

    SerialDeviceEnumerator.cpp
    ${SER_DEV_ENUMERATOR_SRC}
    ${VARIANT_PRV_SRC}
    )
	
add_library(comon_lib OBJECT
    ${clear_SRC}
    ${${PROJECT_NAME}_HDRS_PRV}
    )

if(NOT WIN32)
    set_property(TARGET comon_lib PROPERTY
        COMPILE_FLAGS -fPIC)
endif(NOT WIN32)

message(STATUS "3RD_PARTY_LIBS: ${3RD_PARTY_LIBS}")

set(static_lib_name ${PROJECT_NAME}_static)
add_library(${static_lib_name} STATIC
        $<TARGET_OBJECTS:comon_lib>
	${${PROJECT_NAME}_SRCS}
	)
if(JSONCPP_FOUND)
	target_link_libraries(${static_lib_name} 
		LINK_PRIVATE
			${3RD_PARTY_LIBS}
			${ADD_LIBS}
			${JSONCPP_LIBRARIES}
		)
else(JSONCPP_FOUND)
	target_link_libraries(${static_lib_name} 
		LINK_PRIVATE
			${3RD_PARTY_LIBS}
			${ADD_LIBS}
			libjsoncpp_static
		)
endif(JSONCPP_FOUND)

#Экспортируем знание об инклюд-директориях в область видимости cmake
TARGET_INCLUDE_DIRECTORIES(${static_lib_name} PUBLIC
	$<BUILD_INTERFACE:${JSON_CPP_INCLUDES}>
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
	)

#собрать либу с имянем ${PROJECT_NAME} из...
add_library(${PROJECT_NAME} SHARED 
    $<TARGET_OBJECTS:comon_lib>
    ${${PROJECT_NAME}_SRCS}
    )

if(JSONCPP_FOUND)
	target_link_libraries(${PROJECT_NAME} 
		LINK_PRIVATE # cmake --help-policy CMP0022
			${3RD_PARTY_LIBS}
			${ADD_LIBS}
		LINK_PUBLIC
			${JSONCPP_LIBRARIES}
		)
else(JSONCPP_FOUND)
	target_link_libraries(${PROJECT_NAME} 
		LINK_PRIVATE
			${3RD_PARTY_LIBS}
			${ADD_LIBS}
                        jsoncpp
		)
endif(JSONCPP_FOUND)

# генерация документации
include(GenDoc)

SET(DOXY_OUTPUT_LANGUAGE "Russian")
SET(DOXY_INPUT ${PROJECT_SOURCE_DIR})

SET(DOXY_ENABLED_SECTIONS "developer_sec")
SET(DOXY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/doc")
ADD_DOCUMENTATION(developer_doc Doxyfile)

ADD_CUSTOM_TARGET(doc DEPENDS developer_doc)

########### install ###############
SET(LIB_SUFFIX "" CACHE STRING
    "Optional arch-dependent suffix for the library installation directory")

SET(RUNTIME_INSTALL_DIR bin
    CACHE PATH "Install dir for executables and dlls")
SET(ARCHIVE_INSTALL_DIR lib${LIB_SUFFIX}
    CACHE PATH "Install dir for static libraries")
SET(LIBRARY_INSTALL_DIR lib${LIB_SUFFIX}
    CACHE PATH "Install dir for shared libraries")
SET(INCLUDE_INSTALL_DIR include/${PROJECT_NAME}
    CACHE PATH "Install dir for headers")
SET(REQ_INCLUDE_INSTALL_DIR ${INCLUDE_INSTALL_DIR}/requests
    CACHE PATH "Install dir for request headers")
SET(PACKAGE_INSTALL_DIR lib${LIB_SUFFIX}/cmake
    CACHE PATH "Install dir for cmake package config files")
MARK_AS_ADVANCED(RUNTIME_INSTALL_DIR
    ARCHIVE_INSTALL_DIR
    INCLUDE_INSTALL_DIR
    PACKAGE_INSTALL_DIR
    )

set_target_properties(${PROJECT_NAME} PROPERTIES 
    RESOURCE "${MEMMAPS}"
    PUBLIC_HEADER "${${PROJECT_NAME}_HDRS}"
    PRIVATE_HEADER "${REQUEST_HADERS}"
    )

INSTALL(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}_EXPORT
    RUNTIME 		DESTINATION "${RUNTIME_INSTALL_DIR}"
    LIBRARY 		DESTINATION "${LIBRARY_INSTALL_DIR}"
    ARCHIVE 		DESTINATION "${ARCHIVE_INSTALL_DIR}"
    PUBLIC_HEADER 	DESTINATION "${INCLUDE_INSTALL_DIR}"
    RESOURCE 		DESTINATION "${MEMMAP_PATH_STANDART}"
    PRIVATE_HEADER	DESTINATION "${REQ_INCLUDE_INSTALL_DIR}"
    )

IF(SCTBMbD_WITH_PKGCONFIG_SUPPORT)
    CONFIGURE_FILE(
	"pkg/pkg-config-pattern.pc.in"
	"${CMAKE_BINARY_DIR}/pkg-config/${PROJECT_NAME}.pc"
	@ONLY)
    INSTALL(FILES "${CMAKE_BINARY_DIR}/pkg-config/${PROJECT_NAME}.pc"
	DESTINATION "${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}/pkgconfig")
ENDIF(SCTBMbD_WITH_PKGCONFIG_SUPPORT)

IF(SCTBMbD_WITH_CMAKE_PACKAGE)
    INSTALL(
	EXPORT	    ${PROJECT_NAME}_EXPORT
	DESTINATION ${PACKAGE_INSTALL_DIR}/${PROJECT_NAME}
	FILE        ${PROJECT_NAME}Config.cmake)

    set(CONF_INCLUDE_DIRS
	${CMAKE_INSTALL_PREFIX}/${INCLUDE_INSTALL_DIR}
	${CMAKE_INSTALL_PREFIX}/${REQ_INCLUDE_INSTALL_DIR})

    configure_file(cmake_modules/Config.cmake.in
	"${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config-includes.cmake" @ONLY)

    INSTALL(FILES
	${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config-includes.cmake
	DESTINATION ${PACKAGE_INSTALL_DIR}/${PROJECT_NAME})
ENDIF(SCTBMbD_WITH_CMAKE_PACKAGE)

if(NOT JSONCPP_FOUND)
    ExternalProject_Get_Property(libjsoncpp_build BINARY_DIR)
    if(WIN32)
        add_custom_target(install_jsoncpp
            COMMAND
		"${CMAKE_COMMAND}" "${BINARY_DIR}"
		    -DCMAKE_INSTALL_PREFIX:PATH="${CMAKE_INSTALL_PREFIX}"
                    -DINCLUDE_INSTALL_DIR:PATH="${CMAKE_INSTALL_PREFIX}/include"
                    -DLIBRARY_INSTALL_DIR:PATH="${CMAKE_INSTALL_PREFIX}/lib"
                    -DARCHIVE_INSTALL_DIR:PATH="${CMAKE_INSTALL_PREFIX}/lib"
            COMMAND
                "${CMAKE_COMMAND}" --build "${BINARY_DIR}" --target install
            )
    else(WIN32)
        add_custom_target(install_jsoncpp
	    COMMAND
		"${CMAKE_COMMAND}" "${BINARY_DIR}"
		    -DCMAKE_INSTALL_PREFIX:PATH="${CMAKE_INSTALL_PREFIX}"
            COMMAND
                "${CMAKE_COMMAND}" --build "${BINARY_DIR}" --target install
            COMMAND
                ldconfig
            )
    endif(WIN32)
    install(CODE
	"execute_process(COMMAND
	\"${CMAKE_COMMAND}\" --build . --target install_jsoncpp\)
	"
    )
endif(NOT JSONCPP_FOUND)

########### uninstall ###############
CONFIGURE_FILE(
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
  IMMEDIATE @ONLY)
ADD_CUSTOM_TARGET(uninstall
  "${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake")
