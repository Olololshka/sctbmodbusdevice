/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef PROBER_H
#define PROBER_H

#include <set>

#include <SCTBModbusDevice.h>
#include <Connection.h>

#if !defined(_PTHREAD_H) && ! defined(PTHREAD_H) && !defined(WIN_PTHREADS_H)
extern "C" {
    struct pthread_t;
}
#endif

/** @typedef ProgressInformerCallback
 *  Функция обратного вызова. Информирования о прогрессе сканирования
 *
 *  Информирует пользовательский код о прогрессе выполнения фонового поиска
 *  @param userdata анонимный указатель на пользовательские данные
 *  @param scanPosition адрес просканированного устройства
 */
typedef void (*ProgressInformerCallback)(void* userdata, uint8_t scanPosition);

/** @typedef ScanFinishedInformerCallback
 *  Функция обратного вызова. Сигнал завершения сканирования
 *
 *  Информирует пользовательский код о завершении сканирования
 *  @param userdata анонимный указатель на пользовательские данные
 */
typedef void (*ScanFinishedInformerCallback)(void* userdata);

namespace SCTBModbusDevice
{

class Device;
class AbstractCell;

/** @class Prober
 * Осуществляет фоновый поиск устройств на последовательной шине
 *
 * Для поддержки автоматического обнаружения карта памяти устройства должна
 * содержать ячейку <b>Holding Register</b> c именем <b>"Идентификатор"</b>
 * по любому адресу.\n
 * Сканер осуществляет попытку чтения данной ячейки перебирая адреса на шине,
 * в случае успеха сопоставляется прочитанное значение с ожидаемым в данной
 * ячейке, при совпадении этих значений устройство считается найденным.
 * Список найденный устройств можно получить после завершения поиска.
 */
class SCTB_MODBUS_DEVICE_EXPORT Prober
{
public:
    /** Конструктор
     *  @param _io Инициализированное транспартное соединение Modbus RTU
     *  @param startAddr стартовый адрес для сканирования
     *  @param endAddr коненый адрес для сканирования
     */
    Prober(Connection* _io = NULL, uint8_t startAddr = 1,
           uint8_t endAddr = 0xf7);

    /// Деструктор
    ~Prober();

    /** Установить транспартное соединение для сканирования
     *  @param _io Инициализированное транспартное соединение Modbus RTU
     */
    void setInterface(Connection* _io);

    /** Установить начальный адрес для сканирования
     *  @param startAddr стартовый адрес для сканирования
     */
    void setStartAddr(uint8_t startAddr = 1);

    /** Установить коненый адрес для сканирования
     *  @param endAddr коненый адрес для сканирования
     */
    void setEndAddr(uint8_t endAddr = 0xf7);

    /** Синхронно получить статус сканирования
     *  @return
     *      true - Сканирование выполняется\n
     *      false - Сканирование остановлено
     */
    bool isBusy() const;

    /** Установить время ожидания ответа на сканирующие запросы
     *  @param timeout время ожидания ответа на сканирующие запросы
     */
    void setScanTimeout(Connection::timeval timeout = Connection::timeval(0, 20000));

    /** Получить величину времяни ожидания ответа на сканирующие запросы
     *  @return время ожидания ответа на сканирующие запросы
     */
    Connection::timeval ScanTimeout() const { return scanTimeout; }

    /// Начать фоновое сканирования
    void start();

    /// Принудительно остановить фоновое сканирование
    void stop();

    /// Вызвавший этот метод поток будет ожидать окончания сканирования
    void join();

    /** Получить список обнаруженных при сканировании устройств
     *  @return список обнаруженных при сканировании устройств
     */
    List<Device *> getProbedDevices() const;

    /** Установить функцию обратного вызова информирования о прогрессе
     *  сканирования
     *
     *  Функция будет вызываться каждый раз, когда сканер проверяет новый адрес
     *  @param f указатель на функцию
     *  @param userdata анонимный указатель на пользовательские даные
     */
    void registerProgressInformer(ProgressInformerCallback f, void* userdata);

    /** Установить функцию обратного вызова информирования о завершении
     *  сканирования
     *
     *  Функция будет вызываться после завершения сканирования, но не после
     *  отмены
     *  @param f указатель на функцию
     *  @param userdata анонимный указатель на пользовательские даные
     */
    void registerFinishedCallback(ScanFinishedInformerCallback f, void* userdata);

private:
    ProgressInformerCallback progressInformerCallback;
    void* informerUserData;
    ScanFinishedInformerCallback finishInformerCallback;
    void* finishInformerUserData;

    List<Device*> probedDevices;
    Connection* _io;
    uint8_t startAddr, endAddr;
    pthread_t *thread;

    Connection::timeval scanTimeout;
    Connection::timeval oldtimeout;

    void protectBusy();

    static void *run(void* arg);
    static void threadCleanup(void *arg);

    Map<AbstractCell*, std::set<Device*> > idCellToDevicesMap;
};

}

#endif // PROBER_H
