/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef CELLQUERY_H
#define CELLQUERY_H

#include <SCTBModbusDevice.h>
#include <abstractcell.h>

namespace SCTBModbusDevice {

/** @class CellQuery
 * Функтор выборки ячеек по критерию
 *
 * Позволяет выбрать из набора ячеек только те, которыйе интересуют
 * пользователя используя критерии отбора\n
 * Пример использования доступен в каталоге demo/cellquery
 */
class SCTB_MODBUS_DEVICE_EXPORT CellQuery
{

public:
    CellQuery();

    /// Имя ячейки
    String Name;
    /// Категория
    String Category;
    /// Описание
    String Description;

    /// Искать по части имяни
    bool NameFragment;
    /// Искать по части категории
    bool CategoryFragment;
    /// Искать по части описания
    bool DescriptionFragment;

    /// Тип ячейки (INVALID == любой)
    AbstractCell::enType Type;
    /// Тип значения ячейки (bool, uint16_t, ...)
    std::type_index valueType;


    /** 0 - нет
     *  1 - да
     *  -1 - не важно
     */
    int Serialisable;

    /** Сделать выборку по ячейкам устройства
     *  @param dev устройство, по ячейкам которго сделать выборку
     *  @return Список ячеек, соответствующий указанным критериям
     */
    List<AbstractCell *> operator()(const Device& dev) const;

    /** Сделать выборку по произвольному списку ячеек
     *  @param cellgroup Список ячеек по которым сделать выборку
     *  @return Список ячеек, соответствующий указанным критериям
     */
    List<AbstractCell *> operator()(const List<AbstractCell *>& cellgroup) const;
};

}

#endif // CELLQUERY_H
