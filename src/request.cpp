/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <stdexcept>
#include <cstdarg>

#include "modbus.h"

#include "common.h"

#include "request.h"

struct _libmodbusStatus
{
    char UseCRC;
    void* _compute_meta_length_after_function;
    void* _compute_data_length_after_meta;
};

using namespace SCTBModbusDevice;

request::request(uint8_t slaveAddr, uint16_t startAddr, uint8_t count)
{
    this->slaveAddr = slaveAddr;
    this->startAddr = startAddr;
    this->count = count;
    libmodbusStatus = calloc(sizeof(_libmodbusStatus), 1);
    ((_libmodbusStatus*)libmodbusStatus)->UseCRC = true;
    useCRC = true;
    m_status = NOT_INITIALIZED;
    m_error = modbusException(modbusException::OK);
    m_enableExceptions = false;
}

request::~request()
{
    free(libmodbusStatus);
}

void request::setSlave(uint8_t slaveAddr)
{
    protectBusy();
    this->slaveAddr = slaveAddr;
}

void request::setStartAdress(uint16_t startAddr)
{
    protectBusy();
    this->startAddr = startAddr;
}

void request::setUseCRC(bool useCRC)
{
    protectBusy();
    this->useCRC = useCRC;
}

std::vector<bool> request::toBool() const
{
    std::vector<bool> result;
    for(std::vector<char>::const_iterator it = ansver_data.begin();
        it != ansver_data.end(); ++it)
        result.push_back(*it != 0);
    return result;
}

std::vector<uint16_t> request::toUInt16() const
{
    std::vector<uint16_t> res;
    if (!ansver_data.empty())
    {
        assert(!(ansver_data.size() % sizeof(int16_t)));
        res.resize(ansver_data.size() / sizeof(uint16_t));
        memcpy(res.data(), ansver_data.data(), ansver_data.size());
    }
    return res;
}

std::vector<uint32_t> request::toUInt32() const
{
    std::vector<uint32_t> res;
    if (!ansver_data.empty())
    {
        assert(!(ansver_data.size() % sizeof(int16_t)));
        if (ansver_data.size() % sizeof(int32_t))
            throw std::length_error(
                    common::string_format("Incorrect ansver size to reanslate"
                                          "to Int32 (%d %% %d = %d)",
                                          ansver_data.size(),
                                          sizeof(int32_t),
                                          ansver_data.size() %
                                          sizeof(int32_t)));

        res.resize(ansver_data.size() / sizeof(uint32_t));
        memcpy(res.data(), ansver_data.data(), ansver_data.size());
    }
    return res;
}

std::vector<float> request::toFloat() const
{
    std::vector<float> res;
    if (!ansver_data.empty())
    {
        assert(!(ansver_data.size() % sizeof(int16_t)));
        if (ansver_data.size() % sizeof(float))
            throw std::length_error(
                    common::string_format("Incorrect ansver size to reanslate"
                                          "to float (%d %% %d = %d)",
                                          ansver_data.size(),
                                          sizeof(float),
                                          ansver_data.size() %
                                          sizeof(float)));

        res.resize(ansver_data.size() / sizeof(float));
        memcpy(res.data(), ansver_data.data(), ansver_data.size());
    }
    return res;
}

std::vector<double> request::toDouble() const
{
    std::vector<double> res;
    if (!ansver_data.empty())
    {
        assert(!(ansver_data.size() % sizeof(int16_t)));
        if (ansver_data.size() % sizeof(double))
            throw std::length_error(
                    common::string_format("Incorrect ansver size to reanslate"
                                          "to float (%d %% %d = %d)",
                                          ansver_data.size(),
                                          sizeof(double),
                                          ansver_data.size() %
                                          sizeof(double)));

        res.resize(ansver_data.size() / sizeof(double));
        memcpy(res.data(), ansver_data.data(), ansver_data.size());
    }
    return res;
}

void request::exec(Connection *_io)
{
    setError(modbusException::OK);

    _io->lock();
    _io->connect(slaveAddress());
    _io->flush();

    saveModbussettings(_io);
    process(_io);
    restoreModbussettings(_io);

    _io->unlock();

    m_status = FINISHED;

    if (m_error != modbusException::OK && m_enableExceptions)
        throw m_error;
}

void request::setCount(uint8_t count)
{
    protectBusy();
    this->count = count;
}

void request::protectBusy()
{
    if (m_status == WAITING || m_status == PROCESSING)
        throw std::logic_error("Unable to change request parameters while processing");
}

void request::saveModbussettings(Connection *_io)
{
    _libmodbusStatus *s = (_libmodbusStatus*)libmodbusStatus;
    modbus_t* mb = _io->modbusContext();
    s->UseCRC = modbus_get_use_CRC16(mb);
    s->_compute_data_length_after_meta =
            modbus_get_meta_hook(mb);
    s->_compute_meta_length_after_function =
            modbus_get_function_hook(mb);
}

void request::restoreModbussettings(Connection *_io)
{
    _libmodbusStatus* s = (_libmodbusStatus*)libmodbusStatus;
    modbus_t* mb = _io->modbusContext();
    modbus_set_use_CRC16(mb, s->UseCRC);
    modbus_set_function_hooks(mb, s->_compute_meta_length_after_function,
                              s->_compute_data_length_after_meta);
}

modbusException::modbusException(modbusException::enErrorCode code) :
    std::runtime_error(common::string_format("Modbus error: %s",
                                             modbus_strerror((int)code + MODBUS_ENOBASE)))
{
    err = code;
}

modbusException::modbusException(const modbusException &ref) :
    std::runtime_error(ref.what())
{
    err = ref.err;
}

modbusException::modbusException(int code) :
    std::runtime_error(common::string_format("Modbus error: %s",
                                             modbus_strerror(code)))
{
    int e = code - MODBUS_ENOBASE;
    if (e > EMBBADSLAVE)
        err = UNDEFINED;
    else
        err = (modbusException::enErrorCode)e;
}

template<>
std::vector<bool> convertHalpers::toType<bool>(const request &r)
{
    return r.toBool();
}

template<>
std::vector<int16_t> convertHalpers::toType<int16_t>(const request &r)
{
    std::vector<uint16_t> _v = r.toUInt16();
    std::vector<int16_t> res(_v.size());
    for(unsigned int i = 0; i < _v.size(); ++i)
        res[i] = (int16_t)_v[i];
    return res;
}

template<>
std::vector<int32_t> convertHalpers::toType<int32_t>(const request &r)
{
    std::vector<uint32_t> _v = r.toUInt32();
    std::vector<int32_t> res(_v.size());
    for(unsigned int i = 0; i < _v.size(); ++i)
        res[i] = (int32_t)_v[i];
    return res;
}

template<>
std::vector<float> convertHalpers::toType<float>(const request &r)
{
    return r.toFloat();
}

template<>
std::vector<double> convertHalpers::toType<double>(const request &r)
{
    return r.toDouble();
}

template<>
std::vector<uint16_t> convertHalpers::toType<uint16_t>(const request &r)
{
    return r.toUInt16();
}

template<>
std::vector<uint32_t> convertHalpers::toType<uint32_t>(const request &r)
{
    return r.toUInt32();
}
