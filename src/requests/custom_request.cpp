/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <cassert>
#include <cstring>

#include <pthread.h>

#include "modbus.h"

#include "Connection.h"

#include "custom_request.h"

using namespace SCTBModbusDevice;

CustomRequest::CustomRequest(uint8_t SlaveAddress,
                             const std::vector<char> &req,
                             bool useCRC) :
    request(SlaveAddress, 0)
{
    this->req = req;
    setUseCRC(useCRC);
}

void CustomRequest::setRequest(const std::vector<char> &req)
{
    protectBusy();
    this->req = req;
}

void CustomRequest::process(Connection *_io)
{
    assert(m_status != NOT_INITIALIZED);

    if (req.empty())
    {
        ansver_data.clear();
        return;
    }

    if (_io->isBusy())
        m_status = WAITING;

    modbus_t* mb = _io->modbusContext();

    modbus_set_use_CRC16(mb, IsUseCRC());
    modbus_set_function_hooks(mb, NULL, NULL);

    int writen = modbus_send_raw_request(mb, (uint8_t*)req.data(), req.size());

    if (writen != -1)
    {
        // success
        uint8_t resivebuff[0xff];
        int ressived = modbus_receive_confirmation(mb, resivebuff);
        if (ressived != -1)
        {
            ansver_data.resize(ressived);
            memcpy(ansver_data.data(), resivebuff, ressived);
        }
        else
        {
            setError(modbusException::NO_ANSVER);
            ansver_data.clear();
        }
    }
    else
    {
        // fail
        setError(errno);
    }
}
