/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef READ_SINGLE_COIL_H
#define READ_SINGLE_COIL_H

#include <stdint.h>

#include <request.h>

namespace SCTBModbusDevice {

/**
 * @brief WriteSingleCoil
 * Запрос записи одной катушки
 */
class SCTB_MODBUS_DEVICE_EXPORT WriteSingleCoil : public request
{
public:
    /**
     * @brief Конструктор
     * @param SlaveAddress Адрес устройства, которому предназначен запрос
     * @param adress Адрес катушки
     * @param value значение для записи
     */
    WriteSingleCoil(uint8_t SlaveAddress,
              uint16_t adress = 0, bool value = false);

    /**
     * @brief Изменить значение для записи
     * @param value Новое значение для записи
     */
    void setvalue(bool value);

protected:
    void process(Connection* _io);

private:
    bool value;
};

}

#endif // READ_SINGLE_COIL_H
