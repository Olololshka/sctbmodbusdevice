/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef WRITE_COILS_H
#define WRITE_COILS_H

#include <request.h>

namespace SCTBModbusDevice
{

/**
 * @class WriteCoils
 * Запрос записи катушек
 */
class WriteCoils : public request
{
public:
    /**
     * @brief Конструктор
     * @param SlaveAddress Адрес устройства, которому предназначен запрос
     * @param startAdress Адрес стартовой катушки
     * @param values вектор булевых значений, которые необходимо записать
     */
    WriteCoils(uint8_t SlaveAddress = 0, uint16_t startAdress = 0,
               const std::vector<bool>& values = std::vector<bool>());

    /**
     * @brief Изменить вектор значений для записи
     * @param values Новый вектор булевых значений, которые необходимо записать
     */
    void setvalues(const std::vector<bool>& values);

protected:
    void process(Connection* _io);

private:
    std::vector<bool> values;
};

}

#endif // WRITE_COILS_H
