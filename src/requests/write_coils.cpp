/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <cassert>
#include <cstring>

#include <pthread.h>

#include "modbus.h"

#include "Connection.h"

#include "write_coils.h"

using namespace SCTBModbusDevice;

WriteCoils::WriteCoils(uint8_t SlaveAddress,
                       uint16_t startAdress,
                       const std::vector<bool>& values):
    request(SlaveAddress, startAdress, values.size())
{
    this->values = values;
}

void WriteCoils::setvalues(const std::vector<bool> &values)
{
    protectBusy();
    this->values = values;
}

void WriteCoils::process(Connection *_io)
{
    assert(m_status != NOT_INITIALIZED);

    if (values.empty())
        return;

    if (_io->isBusy())
        m_status = WAITING;

    uint8_t *data = (uint8_t*)malloc(values.size());
    for (unsigned int i = 0; i < values.size(); ++i)
        data[i] = values.at(i) ? TRUE : FALSE;

    int writen = modbus_write_bits(_io->modbusContext(), startAddress(),
                                        values.size(), data);
    if (writen == -1 || (unsigned int)writen != values.size())
        setError(errno); // fail

    free(data);
}

