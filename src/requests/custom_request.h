/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef CUSTOM_REQUEST_H
#define CUSTOM_REQUEST_H

#include <request.h>

struct _modbus;

namespace SCTBModbusDevice
{

/**
 * @class CustomRequest
 * Собственный запрос
 *
 * Позволяет выполнить сконструированный пользователем нестандартный запрос.
 * Запрос состоит из <slaveAddr> <req> [CRC16]
 */
class SCTB_MODBUS_DEVICE_EXPORT CustomRequest : public request
{
public:
    /**
     * @brief Конструктор
     * @param slaveAddr Адрес устройства, которому предназначен запрос
     * @param req вектор байтов, состовляющих тело пользовательского запроса
     * @param useCRC Флаг использования поля контрольной суммы crc16
     */
    CustomRequest(uint8_t SlaveAddress,
                  const std::vector<char>& req,
                  bool useCRC = true);

    /**
     * @brief Изменить тело запроса
     * @param req вектор байтов, состовляющих новое тело запроса
     */
    void setRequest(const std::vector<char>& req);

protected:
    void process(Connection* _io);

private:
    std::vector<char> req;
};

}

#endif // CUSTOM_REQUEST_H
