#include <cstring>

#include "common.h"

#include "Variant.h"


using namespace SCTBModbusDevice;

Variant::Variant()
{
	valType = NONE;
}

Variant::Variant(const Variant& pattern)
{
	valType = pattern.valType;
	storage.resize(pattern.storage.size());
	memcpy(storage.data(), pattern.storage.data(), pattern.storage.size());
}

Variant::Variant(const String & str)
{
	valType = STRING;
	int strlen = str.size() + 1;
	storage.resize(strlen + sizeof(int));
	memcpy(storage.data(), &strlen, sizeof(int));
	memcpy(storage.data() + sizeof(int), str.c_str(), strlen);
}

Variant::Variant(const StringList & sl)
{
	valType = STRINGLIST;
	storage.clear();
	for (StringList::const_iterator it = sl.begin(); it != sl.end(); ++it)
	{
		String s = *it;
		int wp = storage.size();
		storage.resize(storage.size() + s.length());
		memcpy(&storage[wp], s.c_str(), s.length());
		storage.push_back('\0');
	}
	storage.push_back('\0');
}

Variant::Variant(int val)
{
	valType = INT;
	storage.resize(sizeof(int));
	memcpy(storage.data(), &val, sizeof(int));
}

Variant::Variant(bool val)
{
	valType = BOOL;
	storage.resize(1);
	storage[0] = val;
}

Variant::~Variant()
{
}

String Variant::toString() const
{
	String res;
	int t;
	switch (valType)
	{
	case STRING:
		memcpy(&t, storage.data(), sizeof(int));
		res.resize(t - 1);
		memcpy((void*)res.data(), storage.data() + sizeof(int), t - 1);
		break;
	case STRINGLIST:
		res = toStringlist().join(";");
		break;
	case INT:
		memcpy(&t, storage.data(), sizeof(int));
		res = common::string_format("%d", t);
		break;
	case BOOL:
		res = storage[0] ? "true" : "false";
		break;
    default:
        break;
	}
	return res;
}

StringList Variant::toStringlist() const
{
	StringList res;
	String t;
	bool endlinefound = false;
	for (unsigned int i = 0; i < storage.size(); i++)
	{
		char c = storage[i];
		if (c)
		{
			t.push_back(c);
			endlinefound = false;
		}
		else
		{
			if (endlinefound) // double '\0' found
				break;
			res.append(t);
			t.clear();
			endlinefound = true;
		}
	}

	return res;
}
