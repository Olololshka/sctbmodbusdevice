/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
* MODIFIED BY: shiloxyz
*/


#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>

#include <SCTBModbusDevice.h>

extern "C"
{
#include <libudev.h>
}

#include "SerialDeviceEnumerator.h"
#include "serialdeviceenumerator_p.h"

#include <stdio.h>

#ifndef NDEBUG
#include <iostream>
#define qDebug() std::cout
#endif

using namespace SCTBModbusDevice;

SerialDeviceEnumeratorPrivate::SerialDeviceEnumeratorPrivate()
{
    this->udev = ::udev_new();
    if (!this->udev) {
#ifdef qDebug
        qDebug() << "Unix: SerialDeviceEnumeratorPrivate() \n"
                    " -> function: ::udev_new() returned: 0 \n";
#endif
        return;
    }

    this->udev_monitor = ::udev_monitor_new_from_netlink(this->udev, "udev");
    if (!this->udev_monitor) {
#ifdef qDebug
        qDebug() << "Unix: SerialDeviceEnumeratorPrivate() \n"
                    " -> function: ::udev_monitor_new_fronetlink()"
                    " returned: 0 \n";
#endif
        return;
    }

    ::udev_monitor_filter_add_match_subsystem_devtype(this->udev_monitor,
                                                      "tty", 0);
    ::udev_monitor_enable_receiving(this->udev_monitor);
    this->udev_socket = ::udev_monitor_get_fd(this->udev_monitor);

    //Fill the map equivalence name Bus and driver ID.
    this->eqBusDrvMap["usb"] = String("ID_USB_DRIVER");
    //...
    // Fill here for other associates Bus <-> Driver.
    //
    //..............................................

    //Fill mask devices names list specific by OS.
    //Here you add new name unknow device!
#if defined (__unix)
    this->devNamesMask
            << "ttyS"       /* standart UART 8250 and etc. */
            << "ttyUSB"     /* usb/serial converters PL2303 and etc. */
            << "ttyACM"     /* CDC_ACM converters (i.e. Mobile Phones). */
            << "ttyMI"      /* MOXA pci/serial converters. */
            << "rfcomm";    /* Bluetooth serial device. */
    //This add other devices mask.
#endif
}

SerialDeviceEnumeratorPrivate::~SerialDeviceEnumeratorPrivate()
{
    if (-1 != this->udev_socket) {
        //qt_safe_close(this->udev_socket);
        ::close(this->udev_socket);
        this->udev_socket = -1;
    }

    if (this->udev_monitor)
        ::udev_monitor_unref(this->udev_monitor);

    if (this->udev)
        ::udev_unref(this->udev);
}

bool SerialDeviceEnumeratorPrivate::nativeIsBusy() const
{
    bool ret = false;
    String path = this->nativeName();
    if (path.empty())
        return ret;

    /* try open port to detect if busy*/
    FILE* f;
    f = std::fopen(path.data(), "rwb");
    if (f)
        std::fclose(f);
    else
        ret = true;

    return ret;
}

void SerialDeviceEnumeratorPrivate::updateInfo()
{
    SerialInfoMap info;

    struct udev_enumerate *enumerate = ::udev_enumerate_new(this->udev);
    if (!enumerate) {
#ifdef qDebug
        qDebug() << "Unix: SerialDeviceEnumeratorPrivate::updateInfo() \n"
                    " -> function: ::udev_enumerate_new() returned: 0 \n";
#endif
        infoMap = info;
        return;
    }

    struct udev_list_entry *devices, *dev_list_entry;

    ::udev_enumerate_add_match_subsystem(enumerate, "tty");
    ::udev_enumerate_scan_devices(enumerate);

    devices = ::udev_enumerate_get_list_entry(enumerate);

    udev_list_entry_foreach(dev_list_entry, devices) {

        const char *syspath = ::udev_list_entry_get_name(dev_list_entry);
        struct udev_device *udev_device =
                ::udev_device_new_from_syspath(this->udev, syspath);

        if (udev_device) {

            SerialInfo si;
            //get device name
            String s(::udev_device_get_devnode(udev_device));

            for (String mask: this->devNamesMask) {

                if (s.contains(mask)) {

                    //description
                    si.description = String(
                                ::udev_device_get_property_value(
                                    udev_device, "ID_MODEL_FROM_DATABASE"));
                    //revision
                    si.revision = String(
                                ::udev_device_get_property_value(
                                    udev_device, "ID_REVISION"));
                    //bus
                    si.bus = String(
                                ::udev_device_get_property_value(
                                    udev_device, "ID_BUS"));
                    //driver
                    si.driverName =
                            String(
                                ::udev_device_get_property_value(
                                    udev_device,
                                    this->eqBusDrvMap.value(si.bus).
                                    data()));
                    //hardware ID
                    si.hardwareID = StringList();
                    //location info
                    si.locationInfo = String(
                                ::udev_device_get_property_value(
                                    udev_device, "ID_MODEL_ENC"))
                            .replace("\\x20", String(" "));
                    //manufacturer
                    si.manufacturer = String(
                                ::udev_device_get_property_value(
                                    udev_device, "ID_VENDOR_FROM_DATABASE"));
                    //sub system
                    si.subSystem = String(
                                ::udev_device_get_property_value(
                                    udev_device, "SUBSYSTEM"));
                    //service
                    si.service = String();
                    //system path
                    si.systemPath = String(
                                ::udev_device_get_syspath(
                                    udev_device));
                    //product ID
                    si.productID = String(
                                ::udev_device_get_property_value(
                                    udev_device, "ID_MODEL_ID"));
                    //vendor ID
                    si.vendorID = String(
                                ::udev_device_get_property_value(
                                    udev_device, "ID_VENDOR_ID"));
                    //short name
                    si.shortName = String(
                                ::udev_device_get_property_value(
                                    udev_device, "DEVNAME"));
                    //friendly name
                    si.friendlyName = si.description + " (" + si.shortName +")";

                    //add to map
                    info[s] = si;
                }
            }
            ::udev_device_unref(udev_device);
        }
    }

    ::udev_enumerate_unref(enumerate);

    infoMap = info;
}

bool SerialDeviceEnumeratorPrivate::isValid() const
{
    return (this->udev);
}
