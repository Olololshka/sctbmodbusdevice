/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <stdexcept>
#include <sstream>
#include <iostream>

#include <json/json.h>

#include "cell.h"

#include "CellFactory.h"

using namespace SCTBModbusDevice;

AbstractCell *CellFactory::creatyByTypename(const String &Typename)
{
    if (Typename == "uint16_t")
        return new Cell<uint16_t>;
    else if (Typename == "int16_t")
        return new Cell<int16_t>;
    else if (Typename == "uint32_t")
        return new Cell<uint32_t>;
    else if (Typename == "int32_t")
        return new Cell<int32_t>;
    else if (Typename == "float")
        return new Cell<float>;
    else if (Typename == "double")
        return new Cell<double>;
    else if (Typename == "bool")
        return new Cell<bool>;
    else
        throw std::invalid_argument("Invalid cell type: " + Typename);
}

template<typename _cType>
void Set_cell_Borders(AbstractCell* result,
                      const Json::Value & min,
                      const Json::Value & max,
                      const Json::Value & variants,
                      const Json::Value & variantsAdvanced)
{
    Cell<_cType> * t = static_cast<Cell<_cType> *>(result);
    if (!min.isNull())
        t->setMinimum((_cType)min.asDouble());
    if (!max.isNull())
        t->setMaximum((_cType)max.asDouble());
    if (!variants.isNull())
    {
        std::set<_cType> variantsSet;
        for(unsigned int i = 0; i < variants.size(); ++i)
            variantsSet.insert((_cType)variants[i].asDouble());
        t->setValueVariants(variantsSet);
    }
    if (!variantsAdvanced.isNull())
    {
        Map<String, _cType> variantsMap;
        for(unsigned int i = 0; i < variantsAdvanced.size(); ++i)
        {
            Json::Value _Display = variantsAdvanced[i]["Display"];
            Json::Value _value = variantsAdvanced[i]["value"];
            _cType _v;
            if (!_Display.isNull() && !_value.isNull())
            {
                if (_value.isString())
                    _v = (_cType)String(_value.asString()).toInt();
                else
                    _v = (_cType)_value.asDouble();
                variantsMap[_Display.asString()] = _v;
            }
        }
        t->setVariantsAdvanced(variantsMap);
    }
}

AbstractCell *CellFactory::byJson(const Json::Value &definition, bool forceBoolType)
{
    AbstractCell* result;

    if (forceBoolType)
        result = creatyByTypename("bool");
    else
    {
        if (!definition["ValueType"].isNull())
            result = creatyByTypename(definition["ValueType"].asString());
        else
            result = creatyByTypename("uint16_t"); //default
    }

    if (!definition["Address"].isNull())
    {
        if (definition["Address"].isString())
            result->setAddress(String(definition["Address"].asString()).toInt());
        else
            result->setAddress(definition["Address"].asInt());
    }
    else
        result->setAddress(0);

    if (!definition["Access"].isNull())
        result->setReadOnly(!(definition["Access"].asString() == "ReadWrite"));
    else
        result->setReadOnly(true);

    if (!definition["Category"].isNull())
        result->setCategory(definition["Category"].asString());
    if (!definition["Name"].isNull())
        result->setName(definition["Name"].asString());
    if (!definition["Description"].isNull())
        result->setDescription(definition["Description"].asString());
    if (!definition["Serializable"].isNull())
        result->setSerialisable(definition["Serializable"].asBool());

    // value borders
    Json::Value min = definition["MinValue"];
    Json::Value max = definition["MaxValue"];
    Json::Value variants = definition["variants"];
    Json::Value variantsAdvanced = definition["variantsAdvanced"];

    if (result->ValueType() == typeid(uint16_t)) {
        Set_cell_Borders<uint16_t>(result, min, max, variants, variantsAdvanced);
    }
    else if (result->ValueType() == typeid(int16_t)) {
        Set_cell_Borders<int16_t>(result, min, max, variants, variantsAdvanced);
    }
    else if (result->ValueType() == typeid(uint32_t)) {
        Set_cell_Borders<uint32_t>(result, min, max, variants, variantsAdvanced);
    }
    else if (result->ValueType() == typeid(int32_t)) {
        Set_cell_Borders<int32_t>(result, min, max, variants, variantsAdvanced);
    }
    else if (result->ValueType() == typeid(float)) {
        Set_cell_Borders<float>(result, min, max, variants, variantsAdvanced);
    }
    else if (result->ValueType() == typeid(double)) {
        Set_cell_Borders<double>(result, min, max, variants, variantsAdvanced);
    }

    return result;
}
