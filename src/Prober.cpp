/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <stdexcept>
#include <iostream>
#include <fstream>

#include "pthread.h"

#include "DeviceFactory.h"
#include "CellQuery.h"
#include "common.h"
#include "cell.h"

#include "config.h"

#include "Prober.h"

using namespace SCTBModbusDevice;

Prober::Prober(Connection* _io, uint8_t startAddr, uint8_t endAddr)
{
    thread = NULL;
    progressInformerCallback = NULL;
    finishInformerCallback = NULL;

    setInterface(_io);
    setStartAddr(startAddr);
    setEndAddr(endAddr);
    setScanTimeout();

    // get list of known devices
    StringList knownDevicesMaps = DeviceFactory::knownMemmaps();
    if (knownDevicesMaps.empty())
        throw std::runtime_error("No device memory maps avalable!");
    // create devices
    List<Device *> knownDevices;
    for(StringList::const_iterator it = knownDevicesMaps.begin();
        it != knownDevicesMaps.end(); ++it)
    {
        std::filebuf fb;
        String name(*it);
        if (!fb.open(name, std::ios::in))
        {
            std::cerr << "Failed to open mapfile:" << name << std::endl;
            continue;
        }
        std::istream is(&fb);
        Device *d = DeviceFactory::create(is);
        fb.close();
        d->setConnection(this->_io);
        knownDevices << d;
    }
    // create map Device -> idcell
    Map<Device*, AbstractCell*> DeviceToIDCellMap;
    CellQuery idquery;
    idquery.Name = MEMMAP_ID_CELL_NAME;

    for(List<Device *>::const_iterator it = knownDevices.begin();
        it != knownDevices.end(); ++it)
    {
        Device& d = **it;
        List<AbstractCell*> idcellList = idquery(d);
        if (idcellList.empty())
        {
            // no id cell found, remove
            delete *it;
            knownDevices.remove(*it);
            continue;
        }
        DeviceToIDCellMap[*it] = idcellList.at(0);
    }
    // reform to map cell -> deviceList
    for(Map<Device*, AbstractCell*> ::const_iterator DevIdCell =
        DeviceToIDCellMap.begin();
        DevIdCell != DeviceToIDCellMap.end(); ++DevIdCell)
    {
        List<AbstractCell*> keys = idCellToDevicesMap.keys();
        if (keys.empty())
        {
            std::set<Device*> set;
            set.insert(DevIdCell->first);
            idCellToDevicesMap[DevIdCell->second] = set;
        }
        for (List<AbstractCell*>::const_iterator idCell = keys.begin();
             idCell != keys.end(); ++idCell)
        {
            if (**idCell == *DevIdCell->second)
                idCellToDevicesMap[*idCell].insert(DevIdCell->first);
            else
            {
                std::set<Device*> set;
                set.insert(DevIdCell->first);
                idCellToDevicesMap[*idCell] = set;
            }
        }
    }
}

Prober::~Prober()
{
    common::deleteAll(probedDevices.begin(),
                      probedDevices.end());

    for (Map<AbstractCell *, std::set<Device *> >::const_iterator itm
         = idCellToDevicesMap.begin(); itm != idCellToDevicesMap.end(); ++itm)
    {
        std::set<Device*> devs = itm->second;
        common::deleteAll(devs.begin(), devs.end());
    }
}

void Prober::setInterface(Connection *_io)
{
    protectBusy();
    this->_io = _io;
}

void Prober::setStartAddr(uint8_t startAddr)
{
    protectBusy();
    this->startAddr = startAddr;
}

void Prober::setEndAddr(uint8_t endAddr)
{
    protectBusy();
    if (endAddr > 0xf7)
        endAddr = 0xf7;
    this->endAddr = endAddr;
}

bool Prober::isBusy() const
{
    return thread != NULL;
}

void Prober::setScanTimeout(Connection::timeval timeout)
{
    protectBusy();
    this->scanTimeout = timeout;
}

void Prober::start()
{
    protectBusy();
    if (!_io)
        throw std::invalid_argument("Interface must be set before start()");

    thread = (pthread_t*)malloc(sizeof(pthread_t));
    if (pthread_create(thread, NULL, run, this))
        throw std::runtime_error("Failed to create probe thread");
}

void Prober::stop()
{
    if (isBusy())
        pthread_cancel(*thread);
}

void Prober::join()
{
    if (isBusy())
        pthread_join(*thread, NULL);
}

List<Device *> Prober::getProbedDevices() const
{
    List<Device*> result;
    for (List<Device*>::const_iterator it = probedDevices.begin();
         it != probedDevices.end(); ++it)
        result << (*it)->clone();
    return result;
}

void Prober::registerProgressInformer(ProgressInformerCallback f, void *userdata)
{
    protectBusy();
    progressInformerCallback = f;
    informerUserData = userdata;
}

void Prober::registerFinishedCallback(ScanFinishedInformerCallback f, void *userdata)
{
    finishInformerCallback = f;
    finishInformerUserData = userdata;
}

void Prober::protectBusy()
{
    if (isBusy())
        throw std::logic_error("Prober is busy!");
}

void *Prober::run(void *arg)
{
    Prober* _this = (Prober*)arg;

    pthread_cleanup_push(threadCleanup, arg);

    _this->oldtimeout = _this->_io->responceTimeout();
    _this->_io->setResponseTimeout(_this->scanTimeout);
    common::deleteAll(_this->probedDevices.begin(),
                      _this->probedDevices.end());
    _this->probedDevices.clear();

    // scan bus
    List<AbstractCell*> scanCells = _this->idCellToDevicesMap.keys();
    for(uint8_t addr = _this->startAddr; addr <= _this->endAddr; ++addr)
    {
        if (_this->progressInformerCallback)
            _this->progressInformerCallback(_this->informerUserData, addr);
        for(List<AbstractCell*>::const_iterator it = scanCells.begin();
            it != scanCells.end(); ++it)
        {
            pthread_testcancel();
            AbstractCell* cell = *it;
            cell->Parent()->setAddress(addr);
            if (cell->cache(false))
            {
                // кто-то ответил, сравниваем id
                std::type_index ti = cell->ValueType();
                Device* match = NULL;
                std::set<Device*> devs = _this->idCellToDevicesMap[*it];
                for(std::set<Device*>::const_iterator itd = devs.begin();
                    itd != devs.end(); ++itd)
                {
                    Device& checkingDev = **itd;
                    if ((ti == typeid(uint16_t)) &&
                            ((uint16_t)checkingDev.id() ==
                             (static_cast<Cell<uint16_t> *>(cell)->value())))
                        match = *itd;
                    else if ((ti == typeid(uint32_t)) &&
                             (checkingDev.id() ==
                              (static_cast<Cell<uint32_t> *>(cell)->value())))
                        match = *itd;

                    if (match)
                    {
                        // совпадение найдено
                        Device *foundDev = match->clone();
                        foundDev->setAddress(addr);
                        _this->probedDevices << foundDev;
                        // двух девайсов с одинаковым ид быть не может
                        break;
                    }
                }
                if (!match)
                    std::cerr << "Unknown device found at address 0x"
                              << std::hex
                              << String::number((int)addr) << std::endl;
            }
        }
    }
#ifdef WIN_PTHREADS_H
    (*pthread_getclean() = _pthread_cup.next, (_pthread_cup.func((pthread_once_t *)_pthread_cup.arg)));}
#else
     pthread_cleanup_pop(1);
#endif

    return NULL;
}

void Prober::threadCleanup(void *arg)
{
    Prober* _this = (Prober*)arg;

    free(_this->thread);
    _this->thread = NULL;

    _this->_io->setResponseTimeout(_this->oldtimeout);

    if (_this->finishInformerCallback)
        _this->finishInformerCallback(_this->finishInformerUserData);
}

