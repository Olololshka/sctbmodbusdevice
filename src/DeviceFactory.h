/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef DEVICEFACTORY_H
#define DEVICEFACTORY_H

#include <SCTBModbusDevice.h>
#include <Device.h>

namespace Json {
class Value;
}

namespace SCTBModbusDevice {

class AbstractCell;

/**
 * @class DeviceFactory
 * Фабрика устроуств modbus
 */
class SCTB_MODBUS_DEVICE_EXPORT DeviceFactory
{
public:
    //@{
    /// @name Фабричные методы

    /**
     * @brief Создать устройство по идентификатору
     *
     * Ищет карту устройства с указанным идентификатором и создает по нему
     * устройство.
     * @attention Если карты с таким идентификатором не найдено, будет
     * выброшено исключение.
     * @param id Идентификатор
     * @return Созданное устройство
     */
    static Device *create(uint16_t id);

    /**
     * @brief Создать устройство по карте, передаваемой строкой в формате JSON
     * @param memorymap строка в формате JSON, содержащая карту памяти
     * @attention В случае ошибок карты будет выброшено исключение
     * @return Созданное устройство
     */
    static Device *create(const String& memorymap);

    /**
     * @brief Создать устройство по карте, которую необходимо прочитать из
     * потока ввода.
     * @param is поток ввода
     * @attention В случае ошибок карты будет выброшено исключение
     * @return Созданное устройство
     */
    static Device *create(std::istream &is);

    //@}


    /**
     * @class cellConfigurator
     * Функтор для установки дначений по умолчанию для ячеек
     */
    class cellConfigurator
    {
    public:
        /**
         * @brief Сконфигурировать ячейку значениями по-умолчанию
         * @param cell конфигурируемая ячейка
         */
        virtual void operator ()(AbstractCell& cell) const {}

        /**
         * @brief Ячейка типа bool
         * @return возвращает true для гарантированно бинарных ячеек
         */
        virtual bool isBoolType() const { return false; }
    };

    /**
     * @brief Ищет в системе установленные карты памяти и возвращает пути до них
     * @return Список строк, содержащих пути до файлов-карт
     */
    static StringList knownMemmaps();

private:
    static Device *createCommon(const Json::Value &root);
    static List<AbstractCell *> createCellsJsonSection(const Json::Value &value,
                                                Device* dev,
                                                const cellConfigurator& configurator
                                                = cellConfigurator());
};

}

#endif // DEVICEFACTORY_H
