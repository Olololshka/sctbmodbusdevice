#ifndef CELL_H
#define CELL_H

#include <set>
#include <sstream>
#include <iostream>

#include <requests/read_coils.h>
#include <requests/read_discrete_inputs.h>
#include <requests/read_holding_registers.h>
#include <requests/read_input_registers.h>

#include <requests/write_single_coil.h>
#include <requests/write_holding_registers.h>

#include <Device.h>

#include <abstractcell.h>

namespace SCTBModbusDevice {

/**
 * @class Cell
 * Шаблон ячеек
 */
template<typename CellType>
class SCTB_MODBUS_DEVICE_EXPORT Cell : public AbstractCell
{
public:
    /**
     * @brief Конструктор
     * @param parent Устройство, которому принадлежит ячейка
     * @serialisable Флаг запрета группового обращения\n
     *      true - групповой обмен разрешен\n
     *      false - Групповой обмен запрещен
     */
    Cell(Device* parent = NULL, bool serialisable = true) :
        AbstractCell(parent, serialisable)
    {
        minDefined = false;
        maxDefined = false;
        mValType = new std::type_index(typeid(CellType));
    }

    AbstractCell *clone() const
    {
        Cell<CellType> *res = new Cell<CellType>(mParent, mSerialisable);
        res->mAddress = this->mAddress;
        res->mType = this->mType;
        res->mCacheIsValid = false;
        res->mReadonly = this->mReadonly;

        res->mName = this->mName;
        res->mCategory = this->mCategory;
        res->mDescription = this->mDescription;

        res->cachedValue = this->cachedValue;

        res->min = this->min;
        res->max = this->max;
        res->minDefined = this->minDefined;
        res->maxDefined = this->maxDefined;

        res->variants = this->variants;
        res->variantsAdvanced = this->variantsAdvanced;

        return res;
    }

    bool cache(bool verbose = true)
    {
        request *req;
        mCacheIsValid = false;
        switch (Type())
        {
        case HoldingRegister:
            req = new ReadHoldingRegisters(mParent->Address(), mAddress,
                                           typeInfoToSize(ValueType()) / sizeof(uint16_t));
            break;
        case InputRegister:
            req = new ReadInputRegisters(mParent->Address(), mAddress,
                                         typeInfoToSize(ValueType()) / sizeof(uint16_t));
            break;
        case Coil:
            if (ValueType() == typeid(bool))
                req = new ReadCoils(mParent->Address(), mAddress, 1);
            else
                throw std::logic_error("Coil value type set to " +
                                       String(ValueType().name()));
            break;
        case DiscreteInput:
            if (ValueType() == typeid(bool))
                req = new ReadDiscreteInputs(mParent->Address(), mAddress, 1);
            else
                throw std::logic_error("DI value type set to " +
                                       String(ValueType().name()));
            break;
        default:
            throw std::runtime_error("Unknown cell type id=" + String::number(Type()));
        }
        req->enableExceptions(true);

        try {
            Parent()->execRequest(*req);
            cachedValue = req->toType<CellType>().at(0);
            mCacheIsValid = true;
        }
        catch(modbusException e) {
            if (verbose)
                std::cerr << "Read failed: " << e.what() << std::endl;
            cachedValue = 0;
        }

        delete req;

        return mCacheIsValid;
    }

    /**
     * @brief Получить значеине ячейки
     *
     * Если значение не кешировано, будет проведена операция чтения.
     * Если нужно принудительное обновление, используйте @a invalidateCache()
     * @return Значение ячейки
     */
    CellType value()
    {
        if (!mParent)
            throw std::invalid_argument("Cell has no parent!");

        if (!cacheIsValid())
            cache();
        return cachedValue;
    }

    /**
     * @brief Записать новое значение в ячейку
     *
     * @attention Попытка записи в ячейку, которая не поддерживает эту операцию
     * вызовет исключение.
     * Ленивый режим нужен для групповой записи: Обновите содержимое нескольких
     * ячеек в ленивом режиме, а потом воспользуйтесь групповой записью. Однако
     * выйгрыш быдет только в случае, если хотябы некоторые ячейки будут
     * смежными
     * @param val Новое значение
     * @param lazy Ленивый режим
     *  false - записать сразу же
     *  true - для фактической записи требуется вызов метода setValue()
     */
    void setValue(CellType val, bool lazy = false)
    {
        if (!mParent)
            throw std::invalid_argument("Cell has no parent!");
        if ((Type() != HoldingRegister) &&
                (Type() != Coil))
            throw std::logic_error("Cann't write input register.");
        if (IsReadonly())
            throw std::logic_error("Cann't set value for cell with READONLY attribute.");

        // check
        bool checkvalueresult;
        checkvalueresult = (minDefined ? (val > min) : true) &
                (maxDefined ? (val < max) : true);

        if (!variants.empty())
            checkvalueresult |=  variants.find(val) == variants.end();
        if (!variantsAdvanced.empty())
        {
            const List<String> keys = variantsAdvanced.keys();
            typename List<String>::const_iterator findRes = keys.begin();
            for (; findRes != keys.end(); ++findRes)
                if (*findRes == String::number(val))
                    break;
            if (findRes != keys.end())
            {
                val = variantsAdvanced[*findRes];
            }
            checkvalueresult |= findRes != keys.end();
        }

        if (!checkvalueresult)
            throw std::range_error("Can't assign cell to " + String(val));

        mCacheIsValid = false;

        if (lazy)
        {
            cachedValue = val;
            return;
        }

        request *req;

        switch (Type())
        {
        case HoldingRegister:
        {
            size_t size = typeInfoToSize(ValueType());
            std::vector<uint16_t> data(size / sizeof(uint16_t));
            memcpy(data.data(), (const void*)&val, size);
            req = new WriteHoldingRegisters(mParent->Address(), mAddress, data);
        }
            break;
        case Coil:
            req = new WriteSingleCoil(mParent->Address(), mAddress, val);
            break;
        default:
            throw std::runtime_error("Unknown cell type id=" + String::number((size_t)Type()));
        }

        req->enableExceptions(true);
        try
        {
            Parent()->execRequest(*req);
        }
        catch(modbusException e)
        {
            delete req;
            throw e;
        }

        delete req;
    }

    /**
     * @brief Вызывает запись кешированного значения в устройства
     *
     * Используется вместе с ленивым режимом
     */
    void setValue()
    {
        AbstractCell::setValue();
        if (!cacheIsValid())
            setValue(cachedValue);
    }

    /**
     * @brief Установить минимальное значение, которое может содержать ячейка
     * @param minVal Минимально-возможное значение, которое может содержать
     * ячейка
     */
    void setMinimum(const CellType& minVal)
    {
        min = minVal;
        minDefined = true;
    }

    /**
     * @brief Установить максимальное значение, которое может содержать ячейка
     * @param maxVal Максимально-возможное значение, которое может содержать
     * ячейка
     */
    void setMaximum(const CellType& maxVal)
    {
        max = maxVal;
        maxDefined = true;
    }

    /**
     * @brief Установить фиксированный набор вариантов значений ячейки.
     * @param variants Список, содержащий варианты, которые может содержать
     * ячейка
     */
    void setValueVariants(const std::set<CellType>& variants)
    {
        this->variants = variants;
    }

    /**
     * @brief Установить карту расширенных вариантов значения ячейки
     *
     * Карта должна содержать пары <Видимое значение>:<Реальное значение>
     * @param variantsAdvanced Карта расширенных вариантов значений ячейки
     */
    void setVariantsAdvanced(const Map<String, CellType>& variantsAdvanced)
    {
        this->variantsAdvanced = variantsAdvanced;
    }

    String toString() const
    {
        if (minDefined || maxDefined || !variants.empty() || !variantsAdvanced.empty())
        {
            std::stringstream ss;
            ss << AbstractCell::toString();
            if (minDefined)
                ss << ";" << min;
            if (maxDefined)
                ss << ";" << max;
            if (!variants.empty())
            {
                ss << ";[";
                typename std::set<CellType>::const_iterator last = variants.end();
                --last;
                for(typename std::set<CellType>::const_iterator it = variants.begin();
                    it != variants.end(); ++it)
                {
                    ss << String::number(*it);
                    if (it != last)
                        ss << ", ";
                }
                ss << ']';
            }
            if (!variantsAdvanced.empty())
            {
                ss << ";[";
                typename Map<String, CellType>::const_iterator last = variantsAdvanced.end();
                --last;
                for(typename Map<String, CellType>::const_iterator it
                    = variantsAdvanced.begin();
                    it != variantsAdvanced.end(); ++it)
                {
                    ss << "{" << it->first +
                          " : " + String::number(it->second) + "}";
                    if (it != last)
                        ss << ", ";
                }
                ss << ']';
            }
            return ss.str();
        }

        return AbstractCell::toString();
    }

    void setType(enType type)
    {
        AbstractCell::setType(type);
        if (type == AbstractCell::Coil || type == AbstractCell::DiscreteInput)
        {
            if (mValType)
                delete mValType;
            mValType = new std::type_index(typeid(bool));
        }
    }

    unsigned int valueSize() const { return sizeof(CellType); }

    ~Cell() { delete mValType; }

    void fromRaw(const void* d)
    {
        if (typeid(CellType) == typeid(bool))
            cachedValue = (char*)d ? 1 : 0;
        else
            memcpy(&cachedValue, d, sizeof(CellType));

        mCacheIsValid = true;
    }

    size_t writeraw(void* pos)
    {
        if (typeid(CellType) == typeid(bool))
        {
            *((char*)pos) = 1;
            return 1;
        }
        else
        {
            memcpy(pos, &cachedValue, sizeof(CellType));
            return sizeof(CellType);
        }
    }

    String valString()
    {
        std::type_index cellType = ValueType();
        if (cellType == typeid(bool))
            return value() ? "true" : "false";
        else
            return String::number(value());
    }

private:
    CellType cachedValue;

    CellType min, max;
    bool minDefined;
    bool maxDefined;

    std::set<CellType> variants;
    Map<String, CellType> variantsAdvanced;
};

}

#endif // CELL_H
