/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

/**
* @mainpage Руководство программиста SCTBModbusDevice
* @version 0.0.0-git
*
* <hr/>
* \n
* @section SCTBModbusDevice_TOC Содержание
*  -# @ref Intro
*  -# @ref License
*  -# @ref Opportunity
*  -# @ref InstallPage
*   -# @ref InstallPageWindows
*   -# @ref InstallPageLinux
*   -# @ref Config
*
* <hr/>
* \n
* \n
* @section Intro Назначение
* Кросс-платформенная библиотека предназначена для взаимодействия с устройствами
* СКТБ "ЭППА" по протоколу modbus. Позволяет легко интегрировать их в
* вычислительные системы пользователя.
*
* <hr/>
* \n
* \n
* @section License Лицензия
*  Программный проект распространяется на условиях GNU General Public License
* версии 2 (GNU GPL v2)
* @code
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
* @endcode
* \n
* http://www.gnu.org/licenses/gpl-2.0.html
* <hr/>
* \n
* @section Opportunity Возможности
*  -# Поиск доступных в системе последовательных портов и их эмуляторов\n
*       Класс @ref SerialDeviceEnumerator\n
*       Данная часть является модифицированной версией проекта QSerialDevice
*       https://gitorious.org/qserialdevice
* \n
*  -# Установка связи по последовательной шине или при помощи TCP-соединения
*       по протоколу modbus\n
*       Класс @ref Connection\n
*       Использует как backend библиотеку libmodbus
*       https://github.com/ololoshka2871/libmodbus.git (форк)
* \n
*  -# Поиск всех устройсв, подключенных к последовательной шине\n
*       Класс @ref Prober\n
*       Используя карты памяти известных устройств производит их поиск на
*       последовательной шине
* \n
*  -# Простое добавление новых устройств для обслуживания\n
*       Требуется заполнить текстовой файл с картой памяти устройства на языке
*       JSON\n
*       Для разбора карт используется библиотека libjsoncpp
*       https://github.com/open-source-parsers/jsoncpp
* \n
*  -# Объектно-ориентированный подход к представлению устройств как логических
*       структур, состоящих из ячеек @ref Cell различных типов
* \n
*  -# В проекте используются следующие примеры: \n
*   -# SerInfo - Получение информации о последовательных устройствах в системе
* \n
*   -# device - Создание объекта усройств с по известной карте памяти и вывод
*       карты на экран
* \n
*   -# cellquerry - Выборка из устройства различных ячеек при помощи
*       объекта-запроса
* \n
*   -# Probe - Поиск устройств на последовательной шине
* \n
*   -# reader - Цыклическое чтение из одной ячейки устройства
* \n
*   -# reader2 - Цыклическое чтение группы ячеек устройства
*
* <hr/>
* \n
* \n
* @section InstallPage Установка SCTBModbusDevice
* \n
* Сборка библиотеки потребует:
* -# Наличия в системе компилятора языка С/С++ с поддержкой стандарта с++0x
* -# Кросс-платформенной системы сборки cmake (http://www.cmake.org/) версии не
*       ниже 3.1
* -# Системы контроля версй git (http://git-scm.com/)
* -# Соединение с интерентом
*
* Склонируйте репозиторий проекта при помощи git в любой каталог командой
* @code
*   git clone https://Olololshka@bitbucket.org/Olololshka/sctbmodbusdevice.git
* @endcode
* Создайте любой каталог рядом с созданным и перейдите в него, например:
* @code
*   mkdir build-SCTBModbusDevice && cd build-SCTBModbusDevice
* @endcode
*
* Следующие действия различаются для разных операционных систем
* <hr/>
* \n
* @subsection InstallPageWindows Установка под Windows
* <p>
* При установке утилит cmake и git обратите внимание на вопросы о доступности
* их через системную перменную PATH. Проверить корректность установки можно
* командами
* @code
*   cmake --version
*   git --version
* @endcode
* Вы должны получить корректный ответ при их вызове.
* </p>
* \n
* <b>Конфигурирование</b><p>
* Библиотека протестирована с компиляторами Microsoft Visual C и gcc (MinGW)
* По умолчанию cmake подготавливает проект для Microsoft Visual C, для этого
* выполните:
* @code
* cmake ../sctbmodbusdevice
* @endcode
* При желании воспользоваться компилятором gcc выполните
* @code
* cmake ../sctbmodbusdevice -G"MinGW Makefiles"
* @endcode
* </p>
* <b>Сборка</b><p>
* Выполните команду
* @code
* cmake --build .
* @endcode
* Для запуска процесса сборки, в процессе будут скачаны и собраны библиотеки
* libmodbus, libpthreads-win32 и libjsoncpp.\n
* В случае обнаружения наличия в системе установленной libpthreads-win32 или
* libjsoncpp её сборка будет пропущена
* </p>
* <b>Установить полученную библиотеку можно командой</b><p>
* @code
* cmake --build . --target install
* @endcode
* @warning Данная команда копирует результат сборки в каталог Program Files, что
* требует прав учетной записи администартора. Поэтому выполнять данную команду
* следует с правами администартора.
*
* В случае необходимости сборки libjsoncpp она так же будет установлена.
* </p>
* <hr/>
* \n
* \n
* @subsection InstallPageLinux Установка под Linux
* <b>Конфигурирование</b><p>
* Библиотека протестирована с компиляторами gcc clang.\n
* Дополнительно необходимо наличие в системе udev, libpthreads и make, которые,
* вероятно, уже присутствуют в системе по умолчанию.\n
* Также желательно установить libjsoncpp штатным способом. По вопросам установки
* пакетов обратитесь к документации на ваш дистрибутив.\n
* По умолчанию cmake подготавливает проект для gcc, для этого
* выполните:
* @code
* cmake ../sctbmodbusdevice
* @endcode
* При желании воспользоваться компилятором clang выполните
* @code
* cmake ../sctbmodbusdevice -DCMAKE_C_COMPILER=`which clang` \
* -DCMAKE_CXX_COMPILER=`which clang++`
* @endcode
* </p>
* <b>Сборка</b><p>
* Выполните команду
* @code
* make
* @endcode
* Для запуска процесса сборки, в процессе будут скачаны и собраны библиотеки
* libmodbus, libjsoncpp.\n
* В случае обнаружения наличия в системе установленной libjsoncpp её
* сборка будет пропущена.
* </p>
* <b>Установить полученную библиотеку можно командой</b><p>
* @code
* make install
* @endcode
* @warning Данная команда копирует результат сборки в системный каталог
* (по умолчанию /usr/local), что требует прав учетной записи администартора.
* Поэтому выполнять данную команду следует с правами администартора, например
* используя @a sudo\n
* В случае необходимости сборки libjsoncpp она так же будет установлена.
* </p>
* <hr/>
* \n
* \n
* @section Config Настройка SCTBModbusDevice
* cmake позволяет настроить некоторые параметры сборки проекта. Их можно изменит
* выполнив
* @code
* сmake . -D<ИМЯ_ПАРАМЕТРА>=<ЗНАЧЕНИЕ>
* @endcode
* Или при помощи графического редактора
* @code
* сmake-gui .
* @endcode
*
* Список плезных параметров:
* <table>
*   <tr align="left">
*   <th><b>Параметр</b><td><b>Описание</b><td><b>Значение по умолчанию</b>
*   <tr align="left">
*   <th>ROOT_ACCESS         <td>Активирует режим разработчика\n
*                               Использует полные карты устройст для доступа
*                               ко всем ячейкам<td>OFF
*   <tr align="left">
*   <th>CMAKE_INSTALL_PREFIX<td>Базовый каталог для установки Внутри него
*                               будут созданы каталоги bin. lib, share и
*                               include<td><b>Windows x86</b> C:/Program Files/\n
*                                           <b>Windows x86_64</b> C:/Program Files (x86)\n
*                                           <b>Linux</b> /usr/local
*   <tr align="left">
*   <th>SCTBMbD_WITH_CMAKE_PACKAGE
*                           <td>Экспортировать файл конфигурации cmake
*                               при установке.\nОн позволяет легко интегрировать
*                               библиотеку в пользовательское приложение, если
*                               оно использует систему сборки cmake через
*                               find_package()<td>ON
*   <tr align="left">
*   <th>SCTBMbD_WITH_PKGCONFIG_SUPPORT
*                           <td>Экспортировать файл конфигурации pkg-config
*                               при установке.\nОн позволяет интегрировать
*                               библиотеку в пользовательское приложение, если
*                               оно использует систему поиска библиотек
*                               pkg-config<td>OFF
*   <tr align="left">
*   <th>SCTBMbD_WITH_DEMOS<td>Включает режим сборки примеров<td>ON
*   <tr align="left">
*   <th>SCTBMbD_WITH_TESTS<td>Включает режим сборки отладочных
*                                       тестовых программ<td>ON
* </table>
*
* Изменение, сделанные таким образом вступят в силу после пресборки проекта
* */

#ifndef SCTBMODBUSDEVICE_H
#define SCTBMODBUSDEVICE_H

#include <string>
#include <list>
#include <map>
#include <iterator>
#include <sstream>

#if defined(_MSC_VER)
# if defined(DLLBUILD)
/* define DLLBUILD when building the DLL */
#  define SCTB_MODBUS_DEVICE_EXPORT __declspec(dllexport)
# else
#  define SCTB_MODBUS_DEVICE_EXPORT
# endif
#else
# define SCTB_MODBUS_DEVICE_EXPORT
#endif

#define _DECLARE_PRIVATE(Class) \
    inline Class##Private* d_func() { return reinterpret_cast<Class##Private *>(d_ptr); } \
    inline const Class##Private* d_func() const { return reinterpret_cast<const Class##Private *>(d_ptr); } \
    friend class Class##Private;

#define _DISABLE_COPY(Class) \
    Class(const Class &); \
    Class &operator=(const Class &);

namespace SCTBModbusDevice {

/** @Class List
 * Шаблон списка, основанный на списке STL, дополнен некоторыми полезными
 * методами
 */
template <typename T>
class SCTB_MODBUS_DEVICE_EXPORT List : public std::list<T>
{
public:
    /** @brief Метод возвращает ссылку на элемент по номеру в списке
     *  @param pos номер запрашиваемого элемента списка
     *  @return Ссылка на запрашиваемый элемент
     */
    const T & at(int pos) const
    {
        typename List<T>::const_iterator it = this->begin();
        std::advance(it, pos);
        return *it;
    }

    /** @brief Метод проверяет наличие элемента в списке
     *  @param value Ссылка на объект,
     *      вхождение которого в список будет проверено
     *  @return Результат проверки
     *  <table>
     *       <tr><th>true     <td>Объект входит в список
     *       <tr><th>false    <td>Объкта нет в списке
     *  </table>
     */
    bool contains(const T & value) const
    {
        for (typename List<T>::const_iterator it = this->begin();
             it != this->end(); ++it)
            if (*it == value)
                return true;
        return false;
    }

    /** @brief Метод проверяет наличие элемента в списке по указателю
     *  @warning Уместен, только если список содержит указатели на объекты
     *  @param value Ссылка на объект,
     *      вхождение которого в список будет проверено
     *  @return Результат проверки
     *  <table>
     *       <tr><th>true     <td>Объект входит в список
     *       <tr><th>false    <td>Объкта нет в списке
     *  </table>
     */
    bool contains_by_pointer(const T & value) const
    {
        for (typename List<T>::const_iterator it = this->begin();
             it != this->end(); ++it)
            if (**it == *value)
                return true;
        return false;
    }

    /** @brief Добавить элемент в конец списка
     *  Дублирует функциональность std::list::push_back()
     *  @param value Ссылка на значеине, которое необходимо добавить к списку
     *  @return Ссылка на список
     */
    List<T> & operator<<(const T & value)
    {
        this->push_back(value);
        return *this;
    }

    /** @brief Добавить список в конец текущего списка
     *  @param list Список, который будет добавлен в конец текущего
     *  @return Ссылка на список
     */
    List<T> & operator<<(const List<T> & list)
    {
        this->insert(this->end(), list.begin(), list.end());
        return *this;
    }
};


/** @Class Map
 * Шаблон списка, основанный на карте STL, дополнен некоторыми полезными
 * методами
 */
template <typename Key, typename VAL>
class SCTB_MODBUS_DEVICE_EXPORT Map : public std::map<Key, VAL>
{
public:
    /// @brief Возвращает список, содержащий все ключи карты
    List<Key> keys() const
    {
        List<Key> result;
        typename Map<Key, VAL>::const_iterator it = this->begin();
        while(it != this->end())
        {
            result << it->first;
            ++it;
        }
        return result;
    }

    /// @brief Возвращает список, содержащий все значения карты
    List<VAL> values() const
    {
        List<VAL> result;
        typename Map<Key, VAL>::const_iterator it = this->begin();
        while(it != this->end())
        {
            result << it->second;
            ++it;
        }
        return result;
    }

    /** @brief Возвращает значеине из карты по ключу
     *  @param key Ключ карты
     *  @return Значение, если ключ существует, иначе значени по умолчанию для
     *  данной карты
     */
    const VAL value(const Key & key) const
    {
        typename Map<Key, VAL>::const_iterator it = this->begin();
        while(it != this->end())
        {
			if (it->first == key)
				return it->second;
            ++it;
        }
        return VAL();
    }
};

/** @Class Map
 * Шаблон списка, основанный на строке STL, дополнен некоторыми полезными
 * методами
 */
class SCTB_MODBUS_DEVICE_EXPORT String : public std::string
{
public:
    //@{
    /// @name Конструкторы

    /// Конструктор по умолчанию
    String() : std::string() {}

    /// копирующий конструктор
    String(const std::string& str) : std::string(str) {}

    /// конструктор для Си-строки
    String(char* s) : std::string(s ? s : "") {}

    /// конструктор для константной Си-строки
    String(const char* s) : std::string(s ? s : "") {}

    /// констроуктор для строки из массива байт
    String(const char* s, int len) : std::string(s, len) {}

    /// конструктор для преобразования значений типа @a bool в строку
    String(bool b) : std::string(b ? "true" : "false") {}

    //@}

    /** @brief преобразовывает числа в строку по правилам текущей локали
     *  @param i число, подлежащее преобразованию в строку
     *  @return Строковое представление числа, пример:
     *  <table>
     *       <tr><th>"123"
     *       <tr><th>"13.37"
     *       <tr><th>"1.358e+7"
     *  </table>
     */
    template<class T>
    static std::string number(T i)
    {
        std::stringstream ss;
        ss << i;
        return ss.str();
    }

    /** @brief Проверяет вхождение подстоки в строку
     *  @param ссылка на строку, вхождение которой проверяется
     *  @return
     *       <tr><th>true     <td>подстрока @a содержится в строке
     *       <tr><th>false    <td>подстрока @a отсутствует в строке
     *  </table>
     */
    bool contains(const String & str) const;

    /** @brief Заменяет подстроку в строке
     *  @param before Подстрока, которую необходимо заменить
     *  @param after Строка для замены
     *  @return Ссылка на текущую строку
     *  Если Текущая строка не содержит подстроки @a before,
     *  ни чего не произойдет
     */
    String& replace(const String & before, const String & after);

    /** @brief Пытается преобразовать строку в целое число со знаком
     *  Автоматически распознаются следующие префиксы
     *  <table>
     *       <tr><th>без префикса     <td>Десятичное число
     *       <tr><th>0x    <td>Шестнадцатиричное число
     *       <tr><th>0    <td>Восьмиричное число
     *  </table>
     *  @return В случае успеха, возвращает число, соответствующее
     *  строчному представлению, иначе 0
     */
    int toInt() const;
};


/** @Class Map
 * Список строк
 */
class SCTB_MODBUS_DEVICE_EXPORT StringList : public List<String>
{
public:
    //@{
    /// @name Конструкторы

    /// Конструктор по умолчанию
    StringList() : List<String>() {}

    /// Копирующий конструктор
    StringList(const List<String>& sl);

    //@}

    /** Объединяет все строки списка в одну через разделитель
     *  @param separator разделительдля строк списка
     *  @return Строка, состаоящая из элементров списка, разделенных
     *  разделителем @a separator
     */
    String join(const String & separator) const;

    /** Добавляет строку к списку
     *  @param s Добавляемая строка
     *  @return Ссылка на текущий список строк после выполнения операции
     */
    StringList& append(const String& s);
};

}

#endif // SCTBMODBUSDEVICE_H
