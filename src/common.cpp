/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <memory>
#include <cstdarg>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <locale>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <functional>

#ifdef _WIN32
#include <windows.h>
#endif

#include "abstractcell.h"

#include "common.h"

using namespace SCTBModbusDevice;

// http://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf
std::string common::string_format(const std::string fmt_str, ...)
{
#ifdef USE_C11
    /* Reserve two times as much as the length of the fmt_str */
    int final_n, n = ((int)fmt_str.size()) * 2;
    std::string str;
    std::unique_ptr<char[]> formatted;
    va_list ap;
    while(1) {
        /* Wrap the plain char array into the unique_ptr */
        formatted.reset(new char[n]);
        std::strcpy(&formatted[0], fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += std::abs(final_n - n + 1);
        else
            break;
    }
    return std::string(formatted.get());
#else
    int size = ((int)fmt_str.size()) * 2 + 50;   // Use a rubric appropriate for your code
    std::string str;
    va_list ap;
    while (1) {     // Maximum two passes on a POSIX system...
        str.resize(size);
        va_start(ap, fmt_str);
        int n = vsnprintf((char *)str.data(), size, fmt_str.c_str(), ap);
        va_end(ap);
        if (n > -1 && n < size) {  // Everything worked
            str.resize(n);
            return str;
        }
        if (n > -1)  // Needed size returned
            size = n + 1;   // For null char
        else
            size *= 2;      // Guess at a larger size (OS specific)
    }
    return str;
#endif
}

std::string common::make_error_msg(const std::string &msg)
{
    return string_format(msg, strerror(errno));
}

void common::dumpCells(const List<AbstractCell *> &cells)
{
    std::cout << "Cell list dump:" << std::endl;
    for (List<AbstractCell*>::const_iterator it = cells.begin();
         it != cells.end(); ++it)
        std::cout << (*it)->toString() << std::endl;
}

bool String::contains(const String &str) const
{
    return find(str) != std::string::npos;
}

String &String::replace(const String &before, const String &after)
{
    size_t pos = find(before);
    if (pos != std::string::npos)
        std::string::replace(pos, before.length(), after);
    return *this;
}

int String::toInt() const
{
    int res;
    std::stringstream ss;
    if (!strncmp(data(), "0x", 2))
        ss << std::hex << String(&data()[2]);
    else if (data()[0] == '0' && size() > 1)
        ss << std::oct << String(&data()[1]);
    else
        ss << *this;
    ss >> res;
    return res;
}
