/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
*
* MODIFIED BY: shiloxyz
*/

#ifndef SERIALDEVICEENUMERATOR_H
#define SERIALDEVICEENUMERATOR_H

#include <stdint.h>

#include <SCTBModbusDevice.h>

namespace SCTBModbusDevice {

class SerialDeviceEnumeratorPrivate;

/** @Class SerialDeviceEnumerator
 * Перечислитель послдовательных устройств.
 *
 * Синглтон. Позволяет получить список последовательных устройств в системе
 * и информацию о них
 *
 * Пример использования класса для полчения информации об устройствах
 * находится в каталоге demo/SerInfo
 * Доступная информация может отличаться на разных версиях и в разных ОС
 */
class SCTB_MODBUS_DEVICE_EXPORT SerialDeviceEnumerator
{
public:

    /** Получить указатель на экземпляр перечислителя
     * @return Указатель на синглтон перечислителя
     */
    static SerialDeviceEnumerator *instance();
    /** Унечтожить синглтон
     * Используйте только при завершении программы
     */
    static void release();

    /** Получить список всех последовательных устройств в всистеме
     * @return
     * ОС      | Результат
     * --------|-------------------------
     * *nix    | Все устройства /dev/tty*
     * Windows | Все COM-порты
     */

    StringList devicesAvailable() const;

    //@{
    //!  @name Запрос информации

    //Info methods
    /**
     * Установить имя устройства, для получения информации о нем
     * @param name имя устройства, о котором собирается информация
     */
    void setDeviceName(const String &name);
    //

    //! Получить имя устройства
    String name() const;
    //! Получить короткое имя устройства
    String shortName() const;
    //! Получить системный пусть
    String systemPath() const;
    //! Подсистема ядра, обслуживающая устройство
    String subSystem() const;
    //! Информация о положении (если есть)
    String locationInfo() const;
    //! Имя драйвера, обслуживающего устройство
    String driver() const;
    //! Синоним имени (если есть)
    String friendlyName() const;
    //! Описание (если есть)
    String description() const;
    //! Апаратный идентификатор (если есть)
    StringList hardwareID() const;
    //! Уникальный идентификатор производителя
    uint16_t vendorID() const;
    //! Уникальный идентификатор устройства
    uint16_t productID() const;
    //! Производитель (если доступно)
    String manufacturer() const;
    //! Служба (если есть)
    String service() const;
    //! Шина к которой подключено устройство (если доступно)
    String bus() const;
    //! Ревизия устройства (если есть)
    String revision() const;
    //! Подключено-ли устройство в данный момент
    bool isExists() const;
    //! Занято-ли устройство в данный момент
    bool isBusy() const;

    //@}

protected:
    SerialDeviceEnumeratorPrivate * const d_ptr;

private:
    explicit SerialDeviceEnumerator();
    virtual ~SerialDeviceEnumerator();

    static SerialDeviceEnumerator *self;

    _DECLARE_PRIVATE(SerialDeviceEnumerator)
    _DISABLE_COPY(SerialDeviceEnumerator)
};

}

#endif // SERIALDEVICEENUMERATOR_H
