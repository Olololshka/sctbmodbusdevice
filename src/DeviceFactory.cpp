/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <iostream>

#include <dirent.h>

#include <CellFactory.h>

#include <json/json.h>

#include "config.h"

#include "common.h"

#include "DeviceFactory.h"


#ifdef _WIN32
#include <Shlobj.h>
#define PATH_SPLITTER   "\\"
static SCTBModbusDevice::String getHomeDir()
{
    //http://www.cplusplus.com/forum/general/39766/
    WCHAR path[MAX_PATH];
    if (SUCCEEDED(SHGetFolderPathW(NULL, CSIDL_PROFILE, NULL, 0, path)))
    {
        char ch[MAX_PATH];
        char DefChar = ' ';
        WideCharToMultiByte(CP_ACP,0,path,-1, ch,260,&DefChar, NULL);
        return SCTBModbusDevice::String(ch);
    }
    else
        return SCTBModbusDevice::String();
}
#else
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#define PATH_SPLITTER   "/"
static SCTBModbusDevice::String getHomeDir()
{
    struct passwd *pw = getpwuid(getuid());
    return SCTBModbusDevice::String(pw->pw_dir);
}
#endif

using namespace SCTBModbusDevice;

class HoldingCellConfigurator : public DeviceFactory::cellConfigurator
{
public:
    void operator ()(AbstractCell& cell) const
    {
        cell.setType(AbstractCell::HoldingRegister);
    }
};

class InputCellConfigurator : public DeviceFactory::cellConfigurator
{
public:
    void operator ()(AbstractCell& cell) const
    {
        cell.setType(AbstractCell::InputRegister);
        cell.setReadOnly(true);
    }
};

class CoilConfigurator : public DeviceFactory::cellConfigurator
{
public:
    void operator ()(AbstractCell& cell) const
    {
        cell.setType(AbstractCell::Coil);
    }
    bool isBoolType() const { return true; }
};

class DiscreteInputConfigurator : public DeviceFactory::cellConfigurator
{
public:
    void operator ()(AbstractCell& cell) const
    {
        cell.setType(AbstractCell::DiscreteInput);
        cell.setReadOnly(true);
    }
    bool isBoolType() const { return true; }
};

Device *DeviceFactory::create(uint16_t id)
{
    // try find file by id
    const String filename1 =
            common::string_format("%s%X.json", PATH_SPLITTER, id);
    const String filename2 =
            common::string_format("%s%x.json", PATH_SPLITTER, id);

    String usr_path(MEMMAP_PATH_USER);
    usr_path.replace("~", getHomeDir());
    String share_path(MEMMAP_PATH_STANDART);
    share_path.replace("~", getHomeDir());

    std::filebuf fb;

    if (!fb.open(usr_path + filename1, std::ios::in))
        if (!fb.open(usr_path + filename2, std::ios::in))
            if (!fb.open(share_path + filename1, std::ios::in))
                if (!fb.open(share_path + filename2, std::ios::in))
                    throw std::invalid_argument(
                            common::string_format(
                                "No memory map for ID=%X was found.",
                                id));

    // file was found and opened
    std::istream is(&fb);

    Json::Value root;
    Json::Reader reader;
    bool res = reader.parse(is, root);
    fb.close();
    if(!res)
        throw std::runtime_error("Failed to parce memory map: " +
                                 reader.getFormattedErrorMessages());

    return createCommon(root);
}

Device *DeviceFactory::create(const String &memorymap)
{
    Json::Value root;
    Json::Reader reader;
    bool res = reader.parse(memorymap, root);
    if(!res)
        throw std::runtime_error("Failed to parce memory map: " +
                                 reader.getFormattedErrorMessages());

    return createCommon(root);
}

Device *DeviceFactory::create(std::istream &is)
{
    Json::Value root;
    Json::Reader reader;
    bool res = reader.parse(is, root);
    if(!res)
        throw std::runtime_error("Failed to parce memory map: " +
                                 reader.getFormattedErrorMessages());

    return createCommon(root);
}

StringList DeviceFactory::knownMemmaps()
{
    StringList result;

    String usr_path(MEMMAP_PATH_USER);
    usr_path.replace("~", getHomeDir());
    String share_path(MEMMAP_PATH_STANDART);
    share_path.replace("~", getHomeDir());

    StringList dirs;
    dirs << usr_path << share_path;

    DIR *dir;
    struct dirent *ent;
    StringList registred;

    for(StringList::const_iterator it = dirs.begin();
        it != dirs.end(); ++it)
    {
        // http://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c
        if ((dir = opendir (it->c_str())) != NULL)
        {
            /* print all the files and directories within directory */
            while ((ent = readdir (dir)) != NULL)
            {
                // если карта есть в домашнем каталоге, то игнорировать карту
                // из /usr/share
                String name(ent->d_name);
                if (name.contains(".json") && !registred.contains(name))
                {
                    registred << name;
                    result << *it + PATH_SPLITTER + name;
                }
            }
            closedir (dir);
        }
    }

    return result;
}

Device *DeviceFactory::createCommon(const Json::Value &root)
{
    Device *dev;

    const int id = String(root["DevID"].asString()).toInt();
    switch (id)
    {
    default:
        dev = new Device();
    }

    dev->passwordUpdated = false;
    dev->mID = id;
    dev->mClass = root["Class"].asString();
    dev->mDescription = root["Description"].asString();
    if (!root["maxSimulateneousCellsToRead"].isUInt())
        dev->maxSimulateneousCellsToRead = root["maxSimulateneousCellsToRead"].asUInt();
    else
        dev->maxSimulateneousCellsToRead = -1;

    if (!root["useCRC"].isBool())
        dev->setUseCRC(root["useCRC"].asBool());
    else
        dev->setUseCRC(true);

    // load
    dev->cells << createCellsJsonSection(root["HoldingRegisters"], dev, HoldingCellConfigurator());
    dev->cells << createCellsJsonSection(root["InputRegisters"], dev, InputCellConfigurator());
    dev->cells << createCellsJsonSection(root["Coils"], dev, CoilConfigurator());
    dev->cells << createCellsJsonSection(root["DiscreteInputs"], dev, DiscreteInputConfigurator());

    dev->cells.sort(common::PComparer<AbstractCell>);

    return dev;
}

List<AbstractCell *>
DeviceFactory::createCellsJsonSection(const Json::Value &value, Device *dev,
                                      const DeviceFactory::cellConfigurator &configurator)
{
    List<AbstractCell *> result;

    if (!value.isNull())
    {
        List<String> names;
        List<uint8_t> addrs;

        for (unsigned int i = 0; i < value.size(); ++i)
        {
            AbstractCell *cell;
            try {
                cell = CellFactory::byJson(value[i], configurator.isBoolType());
                cell->setParent(dev);
            }
            catch (std::exception e){
                std::cerr << e.what() << std::endl;
                continue;
            }

            if (std::find(names.begin(), names.end(), cell->Name()) != names.end())
                throw std::invalid_argument(common::string_format(
                                                "Device map error: duplicate cells %s", cell->Name().c_str()));

            if (std::find(addrs.begin(), addrs.end(), cell->Address()) != addrs.end())
                throw std::invalid_argument(common::string_format(
                                                "Device map error: duplicate cells with addr 0x%X", cell->Address()));


            configurator(*cell);

            result << cell;
        }
    }

    return result;
}
