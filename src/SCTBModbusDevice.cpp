/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <algorithm>

#include <cell.h>

#include <SCTBModbusDevice.h>

using namespace SCTBModbusDevice;

String StringList::join(const String &separator) const
{
    if (size())
    {
        String result;
        List<String>::const_iterator it = begin();
        do {
            result += *it++;
            if (it == end())
                return result;
            result += separator;
        } while (1);
    }
    else
        return String();
}

StringList &StringList::append(const String &s)
{
    push_back(s);
    return *this;
}

StringList::StringList(const List<String> &sl)
{
    List<String>::const_iterator it = sl.begin();
    while (it != sl.end())
        push_back(*it++);
}
