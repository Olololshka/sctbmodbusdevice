/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <iostream>

#include "requests/read_holding_registers.h"
#include "requests/read_input_registers.h"
#include "requests/read_coils.h"
#include "requests/read_discrete_inputs.h"
#include "requests/write_coils.h"

#include "abstractcell.h"
#include "CellQuery.h"
#include "cell.h"

#include "request.h"

#include "common.h"

#include "config.h"

#include "Device.h"

using namespace SCTBModbusDevice;

static unsigned int cellSizeToMBSize(const AbstractCell* pcell)
{
    if (pcell->ValueType() == typeid(bool))
        return 1;
    else
        return pcell->valueSize() / sizeof(uint16_t);
}

List<AbstractCell *> Device::findSerie(List<AbstractCell *>::const_iterator &pos,
                                       const List<AbstractCell *>::const_iterator &end) const
{
    List<AbstractCell *> result;
    unsigned int Start = (*pos)->Address();
    unsigned int Offset = Start + cellSizeToMBSize(*pos);
    result << *pos;
    ++pos;

    while (pos != end)
    {
        if ((*pos)->Address() == Offset)
        {
            Offset += cellSizeToMBSize(*pos);
            if (Offset - Start > maxSimulateneousCellsToRead)
                break;
            result << *pos;
            ++pos;
        }
        else
            break;
    }

    return result;
}

void Device::RefreshAccessRights()
{
    if (passwordUpdated)
        return;

    CellQuery passwordCellQuerry;
    passwordCellQuerry.Category = MEMMAP_HIDEN_CELL_CATEGORY_NAME;
    passwordCellQuerry.Name = MEMMAP_PASSWD_CELL_NAME;
    passwordCellQuerry.valueType = typeid(uint16_t);
    List<AbstractCell*> passwdCellqr = passwordCellQuerry(cells);
    if (!passwdCellqr.empty())
    {
        // записать пароль в ячейку с паролем
        Cell<uint16_t> *passwdCell =
                static_cast<Cell<uint16_t> *>(passwdCellqr.front());

        passwordUpdated = true;
        passwdCell->setValue(ROOT_PASSWD);
    }
}

request *Device::requestFactory(bool RW, const List<AbstractCell *> cellLst)
{
    request* res = NULL;

    if (!cellLst.empty())
    {
        //type
        switch (cellLst.at(0)->Type()) {
        case AbstractCell::HoldingRegister:
            res = RW ? (request*)new WriteHoldingRegisters() :
                       (request*)new ReadHoldingRegisters();
            break;
        case AbstractCell::InputRegister:
            res = new ReadInputRegisters();
            break;
        case AbstractCell::Coil:
            res = RW ? (request*)new WriteCoils() :
                       (request*)new ReadCoils();
            break;
        case AbstractCell::DiscreteInput:
            res = new ReadDiscreteInputs();
            break;
        default:
            return res;
        }

        int LastvalueSize = cellSizeToMBSize(cellLst.back());

        res->setSlave(mAddress);
        res->setStartAdress(cellLst.front()->Address());
        res->setCount(cellLst.back()->Address() - res->startAddress() +
                      LastvalueSize);
    }
    return res;
}

Device::Device(const Device &pattern)
{
    passwordUpdated = false;
    this->mClass = pattern.mClass;
    this->mDescription = pattern.mDescription;
    this->maxSimulateneousCellsToRead = pattern.maxSimulateneousCellsToRead;
    this->mAddress = pattern.mAddress;
    this->mID = pattern.mID;
    this->io = pattern.io;

    for(List<AbstractCell*>::const_iterator it = pattern.cells.begin();
        it != pattern.cells.end(); ++it)
    {
        AbstractCell* cell = (*it)->clone();
        cell->setParent(this);
        this->cells << cell;
    }
}

Device *Device::clone() const
{
    return new Device(*this);
}

void Device::setAddress(uint8_t addr)
{
    invalideteAll();
    mAddress = addr;
}

void Device::changeAddress(uint8_t newAddr)
{
    CellQuery querry;
    querry.Name = MEMMAP_ADDRESS_CELL_NAME;
    querry.Type = AbstractCell::HoldingRegister;

    List<AbstractCell*> Cells = querry(cells);
    if (Cells.empty())
        throw std::invalid_argument("Unsupported operation");

    AbstractCell* addrCell = Cells.front();
    if(addrCell->ValueType() == typeid(uint16_t))
        static_cast<Cell<uint16_t>*>(addrCell)->setValue(newAddr);
    else if(addrCell->ValueType() == typeid(int16_t))
        static_cast<Cell<int16_t>*>(addrCell)->setValue(newAddr);
    else if(addrCell->ValueType() == typeid(uint32_t))
        static_cast<Cell<uint32_t>*>(addrCell)->setValue(newAddr);
    else if(addrCell->ValueType() == typeid(int32_t))
        static_cast<Cell<int32_t>*>(addrCell)->setValue(newAddr);
    else
        throw std::invalid_argument("Unknown address cell type");

    acceptChanges();

    setAddress(newAddr);
}

void Device::acceptChanges()
{
    if (AcceptCellsCache.empty())
        CacheAcceptCells();

    if (!AcceptCellsCache.empty())
    {
        List<AbstractCell*> clones = common::cloneList(AcceptCellsCache);
        writeCellGroup(clones);
        common::deleteAll(clones.begin(), clones.end());
    }
}

void Device::invalideteAll()
{
    passwordUpdated = false;
    invalideteGroup(cells);
}

void Device::setUseCRC(bool use)
{
    mUseCRC = use;
}

void Device::execRequest(request &req)
{
    RefreshAccessRights();
    req.setUseCRC(mUseCRC);
    req.exec(io);
}

bool Device::readCellGroup(List<AbstractCell *> &cells)
{
    bool ok = true;

    if (cells.empty())
    {
        std::cerr << "Warning: cellgroup to read is empty, nothing to do!";
        return true;
    }

    invalideteGroup(cells);

    List<AbstractCell *> cells_quered;
    CellQuery query;
    query.Serialisable = 0;

    cells_quered = query(cells);
    for(List<AbstractCell *>::const_iterator it = cells_quered.begin();
        it != cells_quered.end(); ++it)
        if (!(*it)->cache())
        {
            std::cerr << "Read failed: "<< std::endl;
            ok = false;
        }

    query.Serialisable = 1;

    for (AbstractCell::enType cellType = AbstractCell::HoldingRegister;
         cellType < AbstractCell::_TypeEnd;
         cellType = (AbstractCell::enType)((int)cellType + 1))
    {
        query.Type = cellType;
        cells_quered = query(cells);
        cells_quered.sort(common::PComparer<AbstractCell>);

        for (List<AbstractCell *>::const_iterator it = cells_quered.begin();
             it != cells_quered.end();)
        {
            List<AbstractCell *>  serie = findSerie(it, cells_quered.end());

            if (serie.empty())
                continue;
            request * req = requestFactory(false, serie);
            req->enableExceptions(true);
            try {
                req->exec(io);
            } catch (modbusException e) {
                std::cerr << "Read failed: " <<e.what() << std::endl;
                ok = false;
                continue;
            }

            // set values
            unsigned int offset = 0;
            for (List<AbstractCell *>::const_iterator it = serie.begin();
                 it != serie.end(); ++it)
            {
                (*it)->fromRaw(&req->rawAnsver()[offset]);
                offset += (*it)->valueSize();
            }
            delete req;
        }
    }

    return ok;
}

void Device::CacheAcceptCells()
{
    CellQuery querry;
    List<AbstractCell*> Cells;

    common::deleteAll(AcceptCellsCache.begin(), AcceptCellsCache.end());
    AcceptCellsCache.clear();

    querry.Name = MEMMAP_SET_SETTINGS_CELL_NAME;
    Cells << querry(cells);

    querry.Name = MEMMAP_WRITE_SETTINGS_CELL_NAME;
    Cells << querry(cells);

    Cells.sort(common::PComparer<AbstractCell>);

    for(List<AbstractCell*>::const_iterator it = Cells.begin();
        it != Cells.end(); ++it)
    {
        AbstractCell* cell = (*it)->clone();
        //TODO спец-значения
        if(cell->ValueType() == typeid(uint16_t))
            static_cast<Cell<uint16_t>*>(cell)->setValue(1, true);
        else if(cell->ValueType() == typeid(int16_t))
            static_cast<Cell<int16_t>*>(cell)->setValue(1, true);
        else if(cell->ValueType() == typeid(uint32_t))
            static_cast<Cell<uint32_t>*>(cell)->setValue(1, true);
        else if(cell->ValueType() == typeid(int32_t))
            static_cast<Cell<int32_t>*>(cell)->setValue(1, true);
        else if(cell->ValueType() == typeid(bool))
            static_cast<Cell<bool>*>(cell)->setValue(true, true);
        else
            throw std::invalid_argument("Unknown accept cell type");
        AcceptCellsCache << cell;
    }
}

bool Device::writeCellGroup(List<AbstractCell *> &cells)
{
    bool ok = true;

    if (cells.empty())
    {
        std::cerr << "Warning: cellgroup to write is empty, nothing to do!";
        return true;
    }

    invalideteGroup(cells);

    List<AbstractCell *> cells_quered;
    CellQuery query;
    query.Serialisable = 0;

    cells_quered = query(cells);
    try
    {
        for(List<AbstractCell *>::const_iterator it = cells_quered.begin();
            it != cells_quered.end(); ++it)
            (*it)->setValue();
    }
    catch(modbusException e)
    {
        std::cerr << "Write failed: " << e.what() << std::endl;
        ok = false;
    }

    query.Serialisable = 1;

    AbstractCell::enType writableCells[] = {AbstractCell::HoldingRegister,
                                         AbstractCell::Coil};
    for (unsigned int i = 0;
         i < sizeof(writableCells) / sizeof(AbstractCell::enType);
         ++i)
    {
        AbstractCell::enType cellType = writableCells[i];
        query.Type = cellType;
        cells_quered = query(cells);
        cells_quered.sort(common::PComparer<AbstractCell>);

        for (List<AbstractCell *>::const_iterator it = cells_quered.begin();
             it != cells_quered.end();)
        {
            List<AbstractCell *>  serie = findSerie(it, cells_quered.end());

            if (serie.empty())
                continue;
            request * req = requestFactory(true, serie);

            if (cellType == AbstractCell::Coil)
            {
                std::vector<bool> requestData(req->Count());
                int c = 0;
                List<AbstractCell *>::const_iterator itC = serie.begin();
                for(;it != serie.end(); ++it, ++c)
                {
                    size_t a;
                    (*itC)->writeraw(&a);
                    requestData[c] = (bool)a;
                }
                static_cast<WriteCoils*>(req)->setvalues(requestData);
            }
            else
            {
                std::vector<uint16_t> requestData(req->Count());
                for(List<AbstractCell *>::const_iterator itH = serie.begin();
                    it != serie.end(); ++it)
                {
                    char *wp = (char*)requestData.data();
                    size_t size = (*itH)->writeraw(wp);
                    wp += size;
                }
                static_cast<WriteHoldingRegisters*>(req)->setvalues(requestData);
            }

            req->enableExceptions(true);
            try {
                execRequest(*req);
                //req->exec(io);
            } catch (modbusException e) {
                std::cerr << "Write failed: " << e.what() << std::endl;
                ok = false;
                continue;
            }
            delete req;
        }
    }

    return ok;
}

Device::~Device()
{
    common::deleteAll(cells.begin(), cells.end());
    common::deleteAll(AcceptCellsCache.begin(), AcceptCellsCache.end());
}

void Device::invalideteGroup(List<AbstractCell *> &cells)
{
    for (List<AbstractCell*>::iterator it = cells.begin();
         it != cells.end(); ++it)
        (*it)->invalidateCache();
}
