/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include "Device.h"

#include "CellQuery.h"

using namespace SCTBModbusDevice;

CellQuery::CellQuery() :
    valueType(typeid(CellQuery))
{
    Type = AbstractCell::INVALID;
    Serialisable = -1;
    NameFragment = false;
    CategoryFragment = false;
    DescriptionFragment = false;
}

List<AbstractCell *> CellQuery::operator ()(const Device& dev) const
{
    return (*this)(dev.Cells());
}

List<AbstractCell *> CellQuery::operator()(const List<AbstractCell *> &cellgroup) const
{
    List<AbstractCell *> result;
    for (List<AbstractCell *>::const_iterator it = cellgroup.begin();
         it != cellgroup.end(); ++it)
    {
        AbstractCell *cell = *it;

        if (!Name.empty())
        {
            if (NameFragment)
            {
                if (!cell->Name().contains(Name))
                    continue;
            }
            else if (Name != cell->Name())
                continue;
        }

        if (!Category.empty())
        {
            if (CategoryFragment)
            {
                if (!cell->Category().contains(Category))
                    continue;
            }
            else if (Category != cell->Category())
                continue;
        }

        if (!Description.empty())
        {
            if (DescriptionFragment)
            {
                if (!cell->Description().contains(Description))
                    continue;
            }
            else if (Description != cell->Description())
                continue;
        }

        if (Type != AbstractCell::INVALID && Type != cell->Type())
            continue;
        if (valueType != typeid(CellQuery) && valueType != cell->ValueType())
            continue;
        if (Serialisable != -1 && (bool)Serialisable != cell->Serialisable())
            continue;

        result << cell;
    }

    return result;
}
