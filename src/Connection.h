/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef _IO_H_
#define _IO_H_

#ifndef MODBUS_H
struct modbus_t;
#endif

#if !defined(_PTHREAD_H) && ! defined(PTHREAD_H) && !defined(_BITS_PTHREADTYPES_H) && !defined(WIN_PTHREADS_H)
extern "C" {
    union pthread_mutex_t;
    struct pthread_t;
}
#endif

#if _WIN32
#include <winsock2.h>
#include <windows.h>
#endif

#include <string>
#include <stdint.h>

#include <SCTBModbusDevice.h>

namespace SCTBModbusDevice {

/** @class Connection
 * Транспортное соединение
 */
class SCTB_MODBUS_DEVICE_EXPORT Connection
{
public:
    /** @class timeval
     *  Аналог Си-структуры timeval
     */
    class timeval
    {
    public:
        //@{
        /// @name Конструкторы
        timeval() {}

        /** @param tv_sec количество секунд
         *  @param tv_usec количество микросекунд
         */
        timeval(uint32_t tv_sec, uint32_t tv_usec)
        { this->tv_sec = tv_sec; this->tv_usec = tv_usec; }

        //@}

        uint32_t tv_sec; /// Количество секунд
        uint32_t tv_usec; /// Количество микросекунд
    };

    //@{
    /// @name Конструкторы

    /** Создать соединение ModBus RTU
     *  @param device имя последовательного устройства
     *  Для windows не следует добавлять \\.\
     *  @param baud скорость
     *  @param parity Одно из значений \n
     *  Настройка | Литера
     *  ----------|-------
     *  Even      | 'E'
     *  Odd       | 'O'
     *  None      | 'N'
     *  @param data_bit количество битов данных
     *  @param stop_bit количество стоп-битов (1 или 2)
     *  @return Объект - транспортное соединение
     */
    static Connection *newRTU(const std::string &device,
                      int baud = 57600, char parity = 'N',
                      int data_bit = 8, int stop_bit = 1);

    /** Создать соединение ModBus TCP
     *  @param ip IP-адрес/DNS-имя сервера modbus tcp
     *  @param port порт сервера modbus tcp
     *  @return Объект - транспортное соединение
     */
    static Connection *newTCP(const std::string &ip, int port);

    //@}

    /// деструктор
    virtual ~Connection();

    /** Изменить режим вывода отладочной информации modbus
     *  @param enabled \n
     *      true - отладочная информация будет выведена в консоль\n
     *      false - отладочная информация скрыта
     */
    void setIODebug(bool enabled);

    /** Установить интервал ожидания ответа после отправки запроса
     *
     *  @attention Разные устройства могут требовать установки рзного периода
     *  ожидания
     *  @param timeout период ожидания ответа с точностью 1ms
     */
    void setResponseTimeout(Connection::timeval timeout);

    /** Получить текущее значение интервала ожидания ответа на запрос
     *  @return Текущее значеине интервала ответа на запрос
     */
    Connection::timeval responceTimeout() const;

    /** Передает соединение во владение вызывающему потоку
     *
     *  Вызвовший поток будет заморожен до того, как соединение не будет
     *  освобождено другим потоком, или мгновенно, если оно было свободно
     */
    void lock();

    /** Освобождает соединение от владения вызывающим потоком
     *  @attention Только владелец может освободить соединение
     */
    void unlock();

    /** Пытается передать соединение во владение вызывающему потоку
     *
     *  @return
     *      true - успешно (поток теперь влядеет соединением)\n
     *      false - Соединение занято другим потоком, неудалось завладеть
     */
    bool tryLock();

    /** Открыть сеанс обмена с ведомым устройством
     *  @param slaveAddr адрес ведомого устройства
     */
    void connect(uint8_t slaveAddr);

    /// Закрыть открытый сеанс обмена
    void close();

    /// Очистить буферы
    void flush();

    /** Проверка "занятости" соединения
     *  @return
     *      true - занято
     *      false - свободно
     */
    bool isBusy();

    /** Получить контекст libmodbus
     *  @return контекст libmodbus
     */
    modbus_t* modbusContext() const;

private:
    Connection();
    modbus_t *mb;

    pthread_mutex_t* mutex;
#if _WIN32
    HANDLE owner;
#else
    pthread_t* owner;
#endif

    int openedAddr;

    void checkOwnerThread() const;
    void setOwner();
    void clearOwner();
};
}

#endif /* _IO_H_ */
