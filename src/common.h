/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef COMMON_H
#define COMMON_H

#include <SCTBModbusDevice.h>

namespace SCTBModbusDevice {

class AbstractCell;

class common
{
public:
    static std::string string_format(const std::string fmt_str, ...);
    static std::string make_error_msg(const std::string& msg);
    static void dumpCells(const List<AbstractCell* > &cells);

    template <typename T>
    static bool PComparer(const T * const & a, const T * const & b)
    {
        return *a < *b;
    }

    template <typename T>
    static void deleteAll(const T& from, const T& end)
    {
        for (T it = from; it != end; ++it)
            delete *it;
    }

    template <typename T>
    static List<T*> cloneList(const List<T*>& l)
    {
        List<T*> res;
        for (typename List<T*>::const_iterator it = l.begin();
             it != l.end(); ++it)
            res << (*it)->clone();
        return res;
    }

private:
    common();
};
}


#endif // COMMON_H
