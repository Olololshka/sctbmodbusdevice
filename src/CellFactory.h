/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef CELLFACTORY_H
#define CELLFACTORY_H

#include <abstractcell.h>

namespace Json {
class Value;
}

namespace SCTBModbusDevice {

/** @calss CellFactory
 * Фабрика ячеек
 *
 * Конструирует ячейки по JSON-описаниям
 */
class CellFactory
{
public:
    /**
     * @brief byJson Создать ячейку по JSON-описанию
     * @param definition JSON-фрагмент карты памяти устройства
     * @param forceBoolType Принудительно установить тип ячейки bool
     * @return Сконструированная ячейка modbus
     */
    static AbstractCell *byJson(const Json::Value& definition,
                                bool forceBoolType = false);

private:
    CellFactory() {}

    static AbstractCell *creatyByTypename(const String& Typename);
    static int AddrStrTou16(const String& addrStr);
};

}

#endif // CELLFACTORY_H
